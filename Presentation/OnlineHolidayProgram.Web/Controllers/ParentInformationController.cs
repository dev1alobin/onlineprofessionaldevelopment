﻿using OHP.Plugin.ParentRegistration.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHolidayProgram.Web.Controllers
{
    public class ParentInformationController : Controller
    {
        #region Properties

        private IParentInformationService parentInfoService;

        #endregion


        #region Constructor

        public ParentInformationController(IParentInformationService parentInfoService)
        {
            this.parentInfoService = parentInfoService;
        }

        #endregion

        #region Methods

        public ActionResult Edit()
        {
            return View();
        }

        #endregion
    }
}