﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHolidayProgram.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("CreateCourse");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CreateCourse()
        {
            return View("CreateCourse");
        }

        public ActionResult Continue()
        {
            return Redirect(VirtualPathUtility.ToAbsolute("~/Parentinformation/ParentInformation/Edit"));
        }

        [AllowAnonymous]
        public ActionResult ActivityCart()
        {
            return View("Cart");
        } 

    }
}