﻿using AloFramework.Auth.Domain;
using AloFramework.Auth.Service;
using AloFramework.Auth.Utilities;
using AloFramework.Common.Services;
using AloFramework.Core.Utilities;
using OHP.Plugin.Auth.Models;
using OHP.Plugin.Auth.Presentation.Models;
using OHP.Plugin.Auth.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace OnlineHolidayProgram.Web.Controllers
{
    public class AccountController : Controller
    {
        #region Properties

        private IAuthService authService;
        private IOHPAuthService ohpAuthService;
        //private IOHPUserSeederService ohpUserSeederService;
        private IInitialNumberService initialNumberService;
        private ISettingService settingService;

        #endregion

        #region Constructor

        public AccountController(IAuthService authService
            , IOHPAuthService ohpAuthService
            //, IOHPUserSeederService ohpUserSeederService
            , IInitialNumberService initialNumberService
            , ISettingService settingService)
        {
            this.authService = authService;
            this.ohpAuthService = ohpAuthService;
            //this.ohpUserSeederService = ohpUserSeederService;
            this.initialNumberService = initialNumberService;
            this.settingService = settingService;
        }

        #endregion

        #region Methods

        // GET: Auth
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            CacheUtil.Set("OHP_PDF_LINK", this.settingService.GetOptionValue("COMMON_OHP_PDF_URL_LINK"));
            return View();
        }

        // POST: Account/Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult doLogin(string Username, string Password, bool RememberMe = false)
        {
            try
            {
                if (this.authService.Authenticate<OHPUser>(Username, Password, RememberMe, GetUserPrincipal))
                {
                    //this.ohpUserSeederService.UserImporter();

                    // TODO: Wether to Redirect to General Information, implement using settings.
                    if (SessionUtil.GetString("LoggedUserType") == "Admin")
                    {
                        return Redirect(VirtualPathUtility.ToAbsolute("~/OHPAdmin/Report/CompleteReport"));
                    }
                    else if (SessionUtil.GetString("LoggedUserType") == "Teacher")
                    {
                        return Redirect(VirtualPathUtility.ToAbsolute("~/MarkRoll/Teacher/RollMark"));
                    }
                    else
                    {
                        return Redirect(VirtualPathUtility.ToAbsolute("~/ProfessionalDevelopment/Course/Index"));
                    }
                    //return Redirect(VirtualPathUtility.ToAbsolute("~/ParentInformation/ParentInformation/Edit"));
                }
                var langObject = LangUtil.GetFreshLangObject("Login");
                SessionUtil.Error((string)langObject.invalidCredentials);
            }
            catch (Exception ex)
            {
                var langObject = LangUtil.GetFreshLangObject("Login");
                SessionUtil.Error((string)langObject.errorTryingToLogin);
                LogUtil.Error("Problem in trying to login, due to " + ex);
            }
            return RedirectToAction("Index");
        }

        private OHPUserPrincipal GetUserPrincipal(OHPUser user)
        {
            var principal = new OHPUserPrincipal(user.Username);
            principal.ReferenceNumber = user.ReferenceNumber;
            Session["ReferenceNumber"] = user.ReferenceNumber;
            Session["LoggedUserDisplayName"] = user.DisplayName;
            Session["LoggedUserType"] = user.UserType;
            Session["LoggedUserID"] = user.UserID;
            CacheUtil.Set("OHP_PDF_LINK", this.settingService.GetOptionValue("COMMON_OHP_PDF_URL_LINK"));
            return principal;
        }

        // GET: SignOut
        public ActionResult SignOut()
        {
            this.authService.SignOut();
            Session.Abandon();
            Session.RemoveAll();
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Register()
        {
            return View(new OHPUserModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(OHPUserModel ohpUserModel)
        {
            try
            {
                if (this.ohpAuthService.ValidateUserModel(ohpUserModel))
                {
                    var referenceNumber = this.initialNumberService.GetNextNumberValue("ReferenceNumber");
                    var userData = new OHPUser
                    {
                        FirstName = ohpUserModel.FirstName,
                        Surname = ohpUserModel.Surname,
                        Username = ohpUserModel.Username,
                        EmailAddress = ohpUserModel.EmailAddress,
                        Password = ohpUserModel.Password,
                        // TODO: Generate Reference Number dynamically
                        ReferenceNumber = referenceNumber,//this.initialNumberService.GetNextNumber("ReferenceNumber").PaddedValue,
                        UserType = "Parent",
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    };
                    this.authService.Register<OHPUser>(userData);
                    
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                var langObject = LangUtil.GetFreshLangObject("OHPRegistration");
                SessionUtil.Error((string)langObject.errorInRegister);
                LogUtil.Error("Problem in registering a new parent, due to " + ex);
            }
            return View(ohpUserModel);
        }

        [AllowAnonymous]
        public ActionResult RunUserImportSeeder()
        {
            try
            {
                //this.ohpUserSeederService.UserImporter();
                return Content("User Imported Successfully");
            }
            catch (Exception ex)
            {
                return Content("Problem while importing seeder, due to " + ex);
            }
        }

        [AllowAnonymous]
        public ActionResult ByPassLogin(string appID, string loginID, string returnURL)
        {
            try
            {
                CacheUtil.Set("OHP_PDF_LINK", this.settingService.GetOptionValue("COMMON_OHP_PDF_URL_LINK"));
                if (AppUtil.GetConfig("AppID").Equals(appID))
                {
                    if (this.ohpAuthService.AuthenticateViaForeignKey(loginID))
                    {
                        return Redirect(VirtualPathUtility.ToAbsolute("~/Home/Index"));
                    }
                    LogUtil.Info("No user found for the ForeignKey given [ForeignKey = " + loginID + "]");
                }
                else {
                    LogUtil.Info("AppID is not valid, Given AppID = " + appID);
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in Authenticating User via ForeignKey, due to " + ex);
            }

            /*if (string.IsNullOrEmpty(returnURL))
                return RedirectToAction("Index");*/
            LogUtil.Info("Return URL = " + returnURL);
            ViewBag.EmailAddress = this.settingService.GetOptionValue("ACCOUNT_SUPPORT_ICT_EMAIL_ADDRESS");
            return View("ByPassLoginReturnPage");
            //return Redirect(returnURL);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetPassword(string emailAddress)
        {
            this.ohpAuthService.ResetPassword(emailAddress);
            return View("ForgotPassword");
        }

        [AllowAnonymous]
        public ActionResult PasswordReset(string resetPasswordToken)
        {
            OHPResetPasswordModel ohpResetPasswordToken = new OHPResetPasswordModel{ ResetPasswordToken = resetPasswordToken };
            return View("ResetPassword", ohpResetPasswordToken);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult DoResetPassword(OHPResetPasswordModel ohpResetPasswordModel)
        {
            try
            {
                if (this.ohpAuthService.ValidateResetPasswordModel(ohpResetPasswordModel))
                {
                    this.ohpAuthService.DoResetPassword(ohpResetPasswordModel);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in resetting password, due to " + ex);
            }
            return View("ResetPassword", ohpResetPasswordModel);
        }

        #endregion
    }
}