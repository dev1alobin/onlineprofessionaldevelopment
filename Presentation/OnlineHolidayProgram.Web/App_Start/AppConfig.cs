﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineHolidayProgram.Web.Config;
using AloFramework.Core.Utilities;

namespace OnlineHolidayProgram.Web.App_Start
{
    public class AppConfig
    {
        #region Methods

        /// <summary>
        /// Initialize the application configuration
        /// </summary>
        public static void RegisterAppConfig()
        {
            AppUtil.SetAppConfig();
        }

        #endregion
    }
}