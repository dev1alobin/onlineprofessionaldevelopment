﻿using AloFramework.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;

namespace OnlineHolidayProgram.Web
{
    public class CultureConfig
    {
        public static void RegisterCulture()
        {
            CultureInfo appCulture = new CultureInfo(AppUtil.GetConfig("CultureInfo"), false);//(CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            appCulture.DateTimeFormat.ShortDatePattern = AppUtil.GetConfig("ShortDatePattern");
            appCulture.DateTimeFormat.LongDatePattern = AppUtil.GetConfig("LongDatePattern");
            appCulture.DateTimeFormat.DateSeparator = "/";
            Thread.CurrentThread.CurrentCulture = appCulture;
            Thread.CurrentThread.CurrentUICulture = appCulture;
        }
    }
}