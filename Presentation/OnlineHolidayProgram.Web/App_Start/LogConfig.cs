﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHolidayProgram.Web
{
    public static class LogConfig
    {
        public static void RegisterLog()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}