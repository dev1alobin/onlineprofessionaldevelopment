﻿using AloFramework.Auth;
using AloFramework.Core;
using AloFramework.Core.Infrastructure;
using AloFramework.DatabaseContext;
using OnlineHolidayProgram.Web.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OHP.Plugin.Auth;
using AloFramework.Common;
using OHP.Plugin.Mail;
using OPD.Plugin.ProfessionalDevelopment;
using OHP.Plugin.Common;

namespace OnlineHolidayProgram.Web
{
    public class ServiceProviderConfig
    {
        public static void RegisterServiceProviders(IApplicationContext appContext)
        {
            /// TODO: declare the list of service providers in a json file..
            appContext.AddServiceProvider(new AppServiceProvider());
            //appContext.AddServiceProvider(new RouteServiceProvider());
            appContext.AddServiceProvider(new DatabaseContextServiceProvider());
            appContext.AddServiceProvider(new AuthServiceProvider());
            appContext.AddServiceProvider(new OHPAuthServiceProvider());
            appContext.AddServiceProvider(new AloFrameworkCommonServiceProvider());
            appContext.AddServiceProvider(new MailServiceProvider());
            appContext.AddServiceProvider(new ProfessionalDevelopmentServiceProvider());
            appContext.AddServiceProvider(new OHPCommonServiceProvider());
        }
    }
}