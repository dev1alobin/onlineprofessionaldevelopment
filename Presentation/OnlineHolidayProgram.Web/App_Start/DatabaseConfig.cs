﻿using AloFramework.Core.Utilities;
using OnlineHolidayProgram.Web.Database.Migration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHolidayProgram.Web
{
    public class DatabaseConfig
    {
        /// <summary>
        /// Execute the migration on startup
        /// </summary>
        public static void MigrateDatabase(string connectionString)
        {
            var migrationNeeded = AppUtil.IsMigrationNeeded();
            if (migrationNeeded)
            {
                CultureConfig.RegisterCulture();
                var migrator = new Migrator(connectionString);
                if (AppUtil.CheckEnv("Development"))
                {
#if DEBUG
                    // This will get executed only when we run in Debug mode not in production
                    migrator.Migrate(runner => runner.MigrateDown(0));
#endif
                }
                migrator.Migrate(runner => runner.MigrateUp());
            }
        }
    }
}