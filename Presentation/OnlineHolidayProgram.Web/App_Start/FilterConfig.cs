﻿using OnlineHolidayProgram.Web.Filters;
using System.Web;
using System.Web.Mvc;

namespace OnlineHolidayProgram.Web
{
    public class FilterConfig
    {
        #region Methods

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new AloAuthorizeAttribute());
            filters.Add(new HandleErrorAttribute());
        }

        #endregion
    }
}
