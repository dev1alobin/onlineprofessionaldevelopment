using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using AloFramework.Core.Infrastructure;

namespace OnlineHolidayProgram.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents(IApplicationContext appContext)
        {

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();


            appContext.RegisterServiceProviders();
            appContext.BootServiceProviders();

            DependencyResolver.SetResolver(new UnityDependencyResolver(appContext.GetContainer()));
        }
    }
}