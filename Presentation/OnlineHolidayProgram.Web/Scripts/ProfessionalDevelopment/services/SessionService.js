﻿; (function ($, window, aloF) {
    'use strict';

    function SessionService() {
        this._initialize();
    }

    SessionService.prototype._initialize = function () {

    }


    SessionService.prototype.getSessionTypes = function (successCallback) {
        $.ajax({
            url: aloF.getBaseURL() + 'ProfessionalDevelopment/SessionType/GetAllSessionTypes',
            success: function (sessionTypes) {
                if (successCallback != null)
                    successCallback(sessionTypes);
            }
        });
    }

    SessionService.prototype.getAllCourses = function (successCallback) {
        $.ajax({
            url: aloF.getBaseURL() + 'ProfessionalDevelopment/Course/GetAllCourses',
            success: function (result) {
                if (successCallback != null)
                    successCallback(result);
            }
        });
    }

    SessionService.prototype.MarkAttendance = function (teamStudentIDs, teamID, successCallback) {
        $.ajax({
            url: aloF.getBaseURL() + 'MarkRoll/Team/MarkAttendance',
            type: 'POST',
            data: {
                "teamStudentIDs": teamStudentIDs,
                "teamID": teamID
            },
            success: function (result) {
                if (successCallback != null)
                    successCallback(result);
            }
        });
    }

    window.SessionService = SessionService;

})(jQuery, window, AloFramework);