﻿; (function ($, window, aloF) {
    'use strict';

    function TeamService() {
        this._initialize();
    }

    TeamService.prototype._initialize = function () {

    }


    TeamService.prototype.getTeamsForTeacher = function (weekID, scheduleID, scheduleBookingDate, teacherID, successCallback) {
        $.ajax({
            url: aloF.getBaseURL() + 'MarkRoll/Teacher/GetTeamsForTeacher',
            data: {
                "WeekID": weekID,
                "scheduleID": scheduleID,
                "scheduleDate": scheduleBookingDate,
                "teacherID": teacherID
            },
            success: function (teams) {
                if (successCallback != null)
                    successCallback(teams);
            }
        });
    }

    TeamService.prototype.getTeamStudents = function (teamID, successCallback) {
        $.ajax({
            url: aloF.getBaseURL() + 'MarkRoll/Teacher/GetTeamStudents',
            data: {
                "teamID": teamID
            },
            success: function (result) {
                if (successCallback != null)
                    successCallback(result);
            }
        });
    }

    TeamService.prototype.MarkAttendance = function (teamStudentIDs, teamID, successCallback) {
        $.ajax({
            url: aloF.getBaseURL() + 'MarkRoll/Team/MarkAttendance',
            type: 'POST',
            data: {
                "teamStudentIDs": teamStudentIDs,
                "teamID": teamID
            },
            success: function (result) {
                if (successCallback != null)
                    successCallback(result);
            }
        });
    }

    window.TeamService = TeamService;

})(jQuery, window, AloFramework);