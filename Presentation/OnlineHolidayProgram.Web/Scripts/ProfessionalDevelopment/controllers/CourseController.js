﻿; (function ($, window) {
    'use strict';
        
    var courseService = new CourseService();

    function CourseController(options) {
        var self = this;
        this.options = $.extend({}, this.options);
        $.extend(this.options, options);
        this._initialize();
        this._events();
    }

    CourseController.prototype.options = {
        weekID: function (value) {
            if (value)
                $("#WeekList").val(value);
            else
                return $("#WeekList").val();
        }
    }

    CourseController.prototype._initialize = function () {
        var self = this;
        self.getScheduleListForWeeksOHPAdmin();
        //self.getAllEnrolledStudents();
        self.initialiseDatePicker();
    }

    

    CourseController.prototype.getScheduleListForWeeksOHPAdmin = function (scheduleCallback) {
        var self = this;
        $("#SelectedStudents").html('<div class=""><h1><i class="fa fa-spinner fa-spin"></i> Loading enrolled students...</h1></div>');
        scheduleService.getScheduleListForWeeksOHPAdmin(
            self.options.weekID()
            , function (schedules) {
                var html = "";//"<option value=''>Select Activity</option>";
                if (schedules.length > 0) {
                    $(schedules).each(function (i, item) {
                        html += "<option value=" + item.ScheduleID + " data-enrolment-capacity=" + item.EnrolmentCapacity + ">" + item.ScheduleName + "</option>";
                    });
                    $("#ActivityList").html(html);
                    self.getAllEnrolledStudents($("#ScheduleBookingDay").val());
                    if (scheduleCallback != null)
                        scheduleCallback();
                }
            });
    }

    CourseController.prototype.initialiseDatePicker = function (studentsCallback) {
        var self = this;
        $('#ScheduleBookingDay').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            lang: 'en',
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                self.getAllEnrolledStudents(ct.dateFormat('d/m/Y'));
            }
        });
    }

    CourseController.prototype.getAllEnrolledStudents = function (scheduleBookingDate, studentsCallback) {
        var self = this;
        $("#SelectedStudents").html('<div class=""><h1><i class="fa fa-spinner fa-spin"></i> Loading enrolled students...</h1></div>');
        scheduleService.getAllEnrolledStudents(
            $("#WeekList").val(),
            $("#ActivityList").val(),
            scheduleBookingDate
            , function (enrolledStudents) {
                var emptyHTML = "<tr><td colspan='2' class='text-center'>No Enrolled Students</td></tr>";
                var html = "";
                $(enrolledStudents).each(function (i, item) {
                    html += "<tr>"
                                + "<td class='text-center'><input type='checkbox' class='enrolled-student' name='StudentInformationID[]' id='StudentInformationID' data-student-id='" + item.StudentInformationID + "' value='" + item.StudentInformationID + "' /></td>"
                                + "<td>" + item.FullName + "</td>"
                                + "</tr>";                    
                });
                if(enrolledStudents.length > 0)
                    $("#EnrolledStudentsTable tbody").html(html);
                else
                    $("#EnrolledStudentsTable tbody").html(emptyHTML);
                /*if (studentsCallback != null)
                    studentsCallback();*/
            });
    }

    CourseController.prototype._events = function () {
        var self = this;
        $("#WeekList").change(function () {
            self.getScheduleListForWeeksOHPAdmin();
        });

        $("#ProgramList").change(function () {
            var programID = $(this).val();
            /// TODO: Get the list of weeks for this program and list out the weeks
            scheduleService.getCurrentProgram(programID, function (program) {
                self.setWeekList(program.Weeks);
            });
        });

        $("#ActivityList").change(function () {
            self.getAllEnrolledStudents($("#ScheduleBookingDay").val());
        });

        $("#EnrolledStudentsTable tbody tr").click(function (event) {

        });

    }

    window.CourseController = CourseController;

})(jQuery, window);