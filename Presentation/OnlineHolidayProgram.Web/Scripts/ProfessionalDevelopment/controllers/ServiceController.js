﻿; (function ($, window) {
    'use strict';
        
    var sessionService = new SessionService();

    function ServiceController(options) {
        var self = this;
        this.options = $.extend({}, this.options);
        $.extend(this.options, options);
        this._initialize();
        this._events();
    }

    ServiceController.prototype.options = {
    }

    ServiceController.prototype._initialize = function () {
        var self = this;
        self.getSessionTypes();
        //self.getAllEnrolledStudents();
        self.getAllCourses();
    }

    

    ServiceController.prototype.getSessionTypes = function (sessionTypeCallback) {
        var self = this;
        sessionService.getSessionTypes(function (sessionTypes) {
                var html = "";//"<option value=''>Select Activity</option>";
                if (sessionTypes.length > 0) {
                    $(sessionTypes).each(function (i, item) {
                        html += "<option value=" + item.SessionTypeID +">" + item.SessionTypeName + "</option>";
                    });
                    $("#SessionTypeID").html(html);
                    if (sessionTypeCallback != null)
                        sessionTypeCallback();
                }
            });
    }

    ServiceController.prototype.initialiseDatePicker = function (studentsCallback) {
        var self = this;
        $('#ScheduleBookingDay').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            lang: 'en',
            closeOnDateSelect: true,
            onChangeDateTime: function (ct, $i) {
                self.getAllEnrolledStudents(ct.dateFormat('d/m/Y'));
            }
        });
    }

    ServiceController.prototype.getAllEnrolledStudents = function (scheduleBookingDate, studentsCallback) {
        var self = this;
        $("#SelectedStudents").html('<div class=""><h1><i class="fa fa-spinner fa-spin"></i> Loading enrolled students...</h1></div>');
        scheduleService.getAllEnrolledStudents(
            $("#WeekList").val(),
            $("#ActivityList").val(),
            scheduleBookingDate
            , function (enrolledStudents) {
                var emptyHTML = "<tr><td colspan='2' class='text-center'>No Enrolled Students</td></tr>";
                var html = "";
                $(enrolledStudents).each(function (i, item) {
                    html += "<tr>"
                                + "<td class='text-center'><input type='checkbox' class='enrolled-student' name='StudentInformationID[]' id='StudentInformationID' data-student-id='" + item.StudentInformationID + "' value='" + item.StudentInformationID + "' /></td>"
                                + "<td>" + item.FullName + "</td>"
                                + "</tr>";                    
                });
                if(enrolledStudents.length > 0)
                    $("#EnrolledStudentsTable tbody").html(html);
                else
                    $("#EnrolledStudentsTable tbody").html(emptyHTML);
                /*if (studentsCallback != null)
                    studentsCallback();*/
            });
    }

    ServiceController.prototype._events = function () {
        var self = this;
        $("#WeekList").change(function () {
            self.getScheduleListForWeeksOHPAdmin();
        });

        $("#ProgramList").change(function () {
            var programID = $(this).val();
            /// TODO: Get the list of weeks for this program and list out the weeks
            scheduleService.getCurrentProgram(programID, function (program) {
                self.setWeekList(program.Weeks);
            });
        });

        $("#ActivityList").change(function () {
            self.getAllEnrolledStudents($("#ScheduleBookingDay").val());
        });

        $("#EnrolledStudentsTable tbody tr").click(function (event) {

        });

    }

    window.ServiceController = ServiceController;

})(jQuery, window);