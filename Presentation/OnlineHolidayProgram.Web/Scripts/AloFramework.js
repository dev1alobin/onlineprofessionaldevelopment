﻿; (function ($, window) {
    'use strict';

    function AloFramework(options) {
        this.options = $.extend({}, this.options);
        $.extend(this.options, options);
        this._initialize();
    }

    AloFramework.prototype.options = {

    }

    AloFramework.prototype._initialize = function () {

    }

    AloFramework.prototype.getBaseURL = function () {
        return location.protocol + "//" + location.hostname +
            (location.port && ":" + location.port) + "/";
        return "";
    }

    AloFramework.prototype.verifyConfirm = function (el) {
        if (el.data('confirm')) {
            if (!confirm(el.data('confirm'))) {
                return false;
            }
            return true;
        }
    }

    $("a[data-method]").on('click', function () {
        var el = $(this);
        var httpMethod = el.data('method').toUpperCase();
        // If the data-method attribute is not PUT or DELETE,
        // then we don't know what to do. Just ignore.
        if ($.inArray(httpMethod, ['PUT', 'DELETE', 'GET']) === -1) {
            return;
        }

        // Allow user to optionally provide data-confirm="Are you sure?"
        if (el.data('confirm')) {
            if (!confirm(el.data('confirm'))) {
                return false;
            }
        }
        var token = '';
        if (el.data('csrf-token')) {
            token = el.data('csrf-token');
        }

        if (el.attr('href')) { // consider it as a tag
            var form = $('<form>', {
                'method': 'POST',
                'action': el.attr('href')
            });

            var token = $('<input>', {
                'type': 'hidden',
                'name': 'csrf_token',
                'value': token
            });

            var hiddenInput = $('<input>', {
                'name': '_method',
                'type': 'hidden',
                'value': el.data('method')
            });

            form.append(token, hiddenInput)
                       .appendTo('body');

            console.log(form.serialize());
            form.submit();
        }
        event.preventDefault();
        return false;
    });

    /**
     * Add the AloFramework to global namespace
     */
    window.AloFramework = new AloFramework({});
})(jQuery, window);