﻿using AloFramework.Core.Utilities;
using OnlineHolidayProgram.Web.Utilities;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace OnlineHolidayProgram.Web.Database.Migration
{
    public class Migrator
    {
        #region Properties

        private string connectionString;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a migrator with connectionString
        /// </summary>
        /// <param name="connectionString"></param>
        public Migrator(string connectionString)
        {
            this.connectionString = connectionString;
        }

        #endregion

        #region InnerClass

        private class MigrationOptions : IMigrationProcessorOptions
        {
            public bool PreviewOnly { get; set; }

            public string ProviderSwitches { get; set; }

            public int Timeout { get; set; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Execute the migration process while application start up
        /// </summary>
        /// <param name="runnerAction"></param>
        public void Migrate(Action<IMigrationRunner> runnerAction)
        {
            var options = new MigrationOptions { PreviewOnly = false, Timeout = 0 };

            var factory = GetMigrationProcessorFactory();
            if (factory != null)
            {
                /// TODO: Get the list of migrations from ServiceProvider and execute it
                /// So it will increase the performance of the application.

                var assemblies = AppDomain.CurrentDomain.GetAssemblies(); // Assembly.GetExecutingAssembly();
                var migrateAssemblies = new FluentMigrator.Infrastructure.AssemblyCollection(assemblies);

                var announcer = new TextWriterAnnouncer(s => LogUtil.Info(s));

                var migrationContext = new RunnerContext(announcer)
                {
#if DEBUG
                    /// TODO: Create some test dummy data.
                    Profile = "Development"
#endif
                };

                var processor = factory.Create(this.connectionString, announcer, options);
                var runner = new MigrationRunner(migrateAssemblies, migrationContext, processor);

                runnerAction(runner);
            }
            else
            {
                LogUtil.Info("Since Migration Fluent DB Factory object is null migration cannot be executed.");
            }
        }

        /// <summary>
        /// Retrieve the object of migration factory class
        /// </summary>
        /// <returns>Returns Migration process factory</returns>
        public MigrationProcessorFactory GetMigrationProcessorFactory()
        {
            var factoryTypeName = AppUtil.GetFluentMigrationDBFactoryType();

            LogUtil.Info("AppUtil.GetFluentMigrationDBFactoryType() =  [" + factoryTypeName ?? "]");
            if (!string.IsNullOrEmpty(factoryTypeName))
            {
                /// TODO: Get the instance dynamically for FluentMigration DB Factory
                var factoryType = new FluentMigrator.Runner.Processors.SqlServer.SqlServer2014ProcessorFactory(); // (MigrationProcessorFactory) Activator.CreateInstance("FluentMigrator.Runner.dll", factoryTypeName).Unwrap();
                LogUtil.Info(factoryTypeName + " object has been created.");
                return factoryType;
            }
            else
            {
                LogUtil.Info("Migration Fluent DB Factory class name is not defined in app settings");
                return null;
            }
        }
        #endregion
    }
}