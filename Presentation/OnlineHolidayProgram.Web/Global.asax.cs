﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AloFramework.Core.Infrastructure;
using OnlineHolidayProgram.Web.Config;
using AloFramework.Core.Utilities;

namespace OnlineHolidayProgram.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region Properties
        /// <summary>
        /// Application Context singleton object
        /// </summary>
        protected IApplicationContext appContext;

        /// <summary>
        /// Connection URL for database access
        /// </summary>
        protected string connectionString;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes ApplicationContext
        /// </summary>
        public MvcApplication()
        {
            this.connectionString = AppUtil.GetDefaultConnectionStr();
            this.appContext = ApplicationContext.GetInstance();
            this.appContext.SetConnectionName(AppUtil.GetConfig(AppMacros.DEFAULT_CONNECTION_NAME));
        }

        #endregion

        #region ApplicationStart

        protected void Application_Start()
        {
            LogConfig.RegisterLog();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);            
            appContext.SetRoutes(RouteTable.Routes);
            appContext.SetViewEngines(ViewEngines.Engines);
            //appContext.SetBundles(BundleTable.Bundles);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Register the ServiceProviders in the application
            ServiceProviderConfig.RegisterServiceProviders(this.appContext);

            // Register Unity Dependency Resolver as default resolver
            UnityConfig.RegisterComponents(this.appContext);

            // Migrate the database 
            DatabaseConfig.MigrateDatabase(this.connectionString);

        }

        #endregion

        #region BeginRequest

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            CultureConfig.RegisterCulture();
        }

        #endregion
    }
}
