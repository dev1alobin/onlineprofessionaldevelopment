﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace OnlineHolidayProgram.Web.Utilities
{
    public static class Utility
    {
        #region Methods

        public static dynamic GetJsonObject(string jsonFilePath)
        {
            if (string.IsNullOrEmpty(jsonFilePath))
                return null;
            var jsonContent = File.ReadAllText(HttpContext.Current.Server.MapPath(jsonFilePath));
            dynamic jsonObject = Newtonsoft.Json.JsonConvert.DeserializeObject(jsonContent);
            return jsonObject;
        }


        #endregion
    }
}