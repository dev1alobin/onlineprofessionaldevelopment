﻿using AloFramework.Core.Auth;
using OHP.Plugin.Auth.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHolidayProgram.Web.Utilities
{
    public class OHPAuthUtil
    {
        /// <summary>
        /// Retrieve the reference number from current logged user
        /// </summary>
        /// <returns>Reference Number of the logged user</returns>
        public static string ReferenceNumber()
        {
            return ((OHPUserPrincipal)HttpContext.Current.User).ReferenceNumber;
        }
    }
}