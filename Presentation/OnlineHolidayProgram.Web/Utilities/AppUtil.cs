﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineHolidayProgram.Web.Utilities;
using OnlineHolidayProgram.Web.Config;
using AloFramework.Core.Utilities;

namespace OnlineHolidayProgram.Web.Utilities
{
    /*internal static class AppUtil
    {
        #region Methods

        /// <summary>
        /// Setup the app config in cache
        /// </summary>
        public static void SetAppConfig()
        {
            var appConfig = Utility.GetJsonObject(AppMacros.GetPath());
            CacheUtil.Set(AppMacros.APP_CACHE_NAME, appConfig);
        }

        public static string GetFluentMigrationDBFactoryType()
        {
            return GetConfig("FluentMigratorDBFactoryType");
        }

        /// <summary>
        /// Checks the config for migration process needed or not
        /// </summary>
        /// <returns>True if NEED_FOR_MIGRATION value is Y</returns>
        public static bool IsMigrationNeeded()
        {
            var needForMigration = GetConfig("NEED_FOR_MIGRATION");
            if (!string.IsNullOrEmpty(needForMigration) && needForMigration.Equals("Y"))
                return true;
            else
                return false;
        }

        public static string GetDefaultConnectionStr()
        {
            return GetConnectionString(GetConfig(AppMacros.DEFAULT_CONNECTION_NAME));
        }

        public static string GetConnectionString(string connectionName)
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
        }

        public static string GetConfig(string configKey)
        {
            var configValue = System.Configuration.ConfigurationManager.AppSettings[configKey];
            if (string.IsNullOrEmpty(configValue))
            {
                LogUtil.Info(configKey + " is empty or not available");
            }
            return configValue;
        }

        public static string GetEnv()
        {
            string env = GetConfig("Environment");
            return string.IsNullOrEmpty(env)? "Default" : env;
        }

        public static bool CheckEnv(string Env)
        {
            return GetEnv().Equals(Env);
        }

        #endregion
    }*/
}