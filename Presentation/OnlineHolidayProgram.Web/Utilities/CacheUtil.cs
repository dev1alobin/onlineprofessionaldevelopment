﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace OnlineHolidayProgram.Web.Utilities
{
    public static class CacheUtil
    {
        #region Methods

        /// <summary>
        /// Set the value of cache
        /// </summary>
        /// <param name="cacheName"></param>
        /// <param name="cacheObject"></param>
        public static void Set(string cacheName, dynamic cacheObject)
        {
            GetCurrentCache()[cacheName] = cacheObject;
        }

        /// <summary>
        /// Get the cache value
        /// You can fetch the cache object property value with dot separator in cache name.
        /// Eg: 
        /// </summary>
        /// <param name="cacheName">cacheName can be split with . for accessing the property of cache object directly and retrive the value</param>
        /// <returns></returns>
        public static object Get(string cacheName)
        {
            if (string.IsNullOrEmpty(cacheName))
                return "";

            var cacheNameInfo = cacheName.Split('.');
            // Checks if the cacheName contains the . separator like appConfig.DefaultLangText
            if (cacheNameInfo.Length == 2)
            {
                return GetCacheProperty(cacheNameInfo);
            }
            else
            {
                return GetCurrentCache()[cacheName];
            }
        }

        public static object GetCacheProperty(string[] cacheNameInfo)
        {
            dynamic cacheObject = GetCurrentCache()[cacheNameInfo[0]];
            var cacheProperty = cacheObject.GetType().GetProperty(cacheNameInfo[1]);
            return cacheProperty.GetValue(cacheObject, null);
        }

        public static void RemoveCache(string cacheName)
        {
            GetCurrentCache().Remove(cacheName);
        }

        public static Cache GetCurrentCache()
        {
            return HttpContext.Current.Cache;
        }

        #endregion
    }
}