﻿using OnlineHolidayProgram.Web.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace OnlineHolidayProgram.Web.Utilities
{
    public static class AuthUtil
    {
        #region Methods

        /// <summary>
        /// Check the authentication of user
        /// </summary>
        /// <returns></returns>
        public static Boolean Check()
        {
            return HttpContext.Current.Request.IsAuthenticated;
        }

        /// <summary>
        /// Retrieve the current logged in username
        /// </summary>
        /// <returns>Username of the logged user</returns>
        public static string Username()
        {
            return HttpContext.Current.User.Identity.Name;
        }
        
        /// <summary>
        /// Verifies the logged user is in specific role or not
        /// </summary>
        /// <param name="role"></param>
        /// <returns>True if current logged user has role provided</returns>
        public static bool IsInRole(string role)
        {
            return HttpContext.Current.User.IsInRole(role);
        }

        public static void Login(string username, bool rememberMe)
        {
            FormsAuthentication.RedirectFromLoginPage(username, rememberMe);
        }

        #endregion
    }
}