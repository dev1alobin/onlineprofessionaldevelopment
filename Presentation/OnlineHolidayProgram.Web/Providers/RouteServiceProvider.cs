﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AloFramework.Core.Infrastructure;
using AloFramework.Core.Container;

namespace OnlineHolidayProgram.Web.Providers
{
    public class RouteServiceProvider : ServiceProvider
    {
        public override void Register(IApplicationContext appContext)
        {
            var routes = appContext.GetRoutes();

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            base.Register(appContext);
        }
    }
}