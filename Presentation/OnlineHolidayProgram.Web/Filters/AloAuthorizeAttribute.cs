﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AloFramework.Core.Auth;

namespace OnlineHolidayProgram.Web.Filters
{
    /// <summary>
    /// Authenticating the user before entering the route
    /// </summary>
    public class AloAuthorizeAttribute : AuthorizeAttribute
    {
        #region Properties

        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AloAuthorizeAttribute));

        protected virtual UserPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as UserPrincipal; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handle Authentication
        /// TODO: Add the authorization process soon with role and permission capability
        /// </summary>
        /// <param name="filterContext"></param>
        /*public override void OnAuthorization(AuthorizationContext filterContext)
        {
            logger.Info("Verifying authorization process");
            if (filterContext.HttpContext.Request.IsAuthenticated && filterContext.HttpContext.Session["LoggedUserDisplayName"] != null)
            {
                logger.Info("User is authenticated");
            }
            else
            {
                logger.Info("Redirecting to login page since user is not authenticated");

                //filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Login", controller = "Account" }));
                base.OnAuthorization(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (System.Web.HttpContext.Current.Session["LoggedUserDisplayName"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Login", controller = "Account", area = "" }));
                logger.Info("The page is redirected to Login page due to session expires.");
            }
        }*/

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if (httpContext.Session == null || httpContext.Session["LoggedUserDisplayName"] == null)
                return false;

            return base.AuthorizeCore(httpContext);
        }
        #endregion
    }
}