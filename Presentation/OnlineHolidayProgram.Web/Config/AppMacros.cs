﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHolidayProgram.Web.Config
{
    public static class AppMacros
    {
        #region Config Properties

        /// <summary>
        /// Default Configuration Folder name
        /// </summary>
        public const string DEFAULT_CONFIG_FOLDER = "Config/";

        /// <summary>
        /// Default app config file name
        /// </summary>
        public const string APP_CONFIG_FILE = "AppConfig.js";

        /// <summary>
        /// Key name of cache for application config object
        /// </summary>
        public const string APP_CACHE_NAME = "AppConfig";

        /// <summary>
        /// Default Connection String
        /// </summary>
        public const string DEFAULT_CONNECTION_STRING = "AloFrameworkContext";

        /// <summary>
        /// Default Connection Name
        /// </summary>
        public const string DEFAULT_CONNECTION_NAME = "defaultConnection";

        #endregion

        #region Config Methods

        public static string GetPath()
        {
            return DEFAULT_CONFIG_FOLDER + APP_CONFIG_FILE;
        }

        #endregion
    }
}