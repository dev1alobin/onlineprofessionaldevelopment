﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OPD.Plugin.ProfessionalDevelopment
{
    public class ProfessionalDevelopmentAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "ProfessionalDevelopment";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "ProfessionalDevelopment_default",
                "ProfessionalDevelopment/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

