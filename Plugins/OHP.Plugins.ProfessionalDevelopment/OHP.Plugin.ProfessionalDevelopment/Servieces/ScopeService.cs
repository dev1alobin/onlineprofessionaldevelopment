﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;
using AloFramework.Core.Utilities;
using AloFramework.Auth.Utilities;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public class ScopeService : IScopeService
    {

        #region Properties

        private IScopeRepository scopeRepo;

        #endregion


        #region Constructor

        public ScopeService(IScopeRepository scopeRepo)
        {
            this.scopeRepo = scopeRepo;
        }

        #endregion


        #region Methods

        public List<Scope> GetScopes()
        {
            var scopes = new List<Scope>();
            try
            {
              
                scopes = this.scopeRepo.GetScopes();
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in retriving Scopes list, due to " + ex);
            }
            return scopes;
        }

      
        #endregion
    }
}
