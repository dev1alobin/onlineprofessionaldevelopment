﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public interface ISessionService
    {

        List<Session> GetAllSessions(SessionListViewModel sessionListViewModel);

        Session GetSession(long sessionID);

        // int SaveSession(Session session, long[] ScopeID, long[] IsMandatory);

        int SaveSession(Session session);

        void Delete(long? sessionID);
        

    }
}
