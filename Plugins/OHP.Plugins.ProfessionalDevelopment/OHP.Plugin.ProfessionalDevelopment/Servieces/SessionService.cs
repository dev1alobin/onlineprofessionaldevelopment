﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;
using AloFramework.Core.Utilities;
using AloFramework.Auth.Utilities;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public class SessionService : ISessionService
    {

        #region Properties

        private ISessionRepository sessionRepo;

        #endregion


        #region Constructor

        public SessionService(ISessionRepository sessionRepo)
        {
            this.sessionRepo = sessionRepo;
        }

        #endregion


        #region Methods

        public List<Session> GetAllSessions(SessionListViewModel sessionListViewModel)
        {
            var sessions = new List<Session>();
            var sessionFilter = sessionListViewModel.SessionFilter;
            try
            {
                if (sessionFilter == null)
                    sessionFilter = new SessionFilterModel();
                sessionListViewModel.SessionFilter = sessionFilter;
                sessions = this.sessionRepo.GetAllSessions(sessionListViewModel);
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in retriving Sessions list, due to " + ex);
            }
            return sessions;
        }

        public Session GetSession(long sessionID)
        {
            return this.sessionRepo.GetSession(sessionID);
        }

        public int SaveSession(Session session)
        {
            try
            {
                LogUtil.Info("Trying to save Session, Session Data = " + Newtonsoft.Json.JsonConvert.SerializeObject(session));
                if (session.SessionID > 0)
                {
                    return this.sessionRepo.Edit(session);
                }
                else
                {
                    return this.sessionRepo.SaveSession(session);
                }

            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in saving Session, due to " + ex);
            }
            return 0;
        }

        public void Delete(long? sessionID) {
            this.sessionRepo.Delete(sessionID);
        }
        #endregion
    }
}
