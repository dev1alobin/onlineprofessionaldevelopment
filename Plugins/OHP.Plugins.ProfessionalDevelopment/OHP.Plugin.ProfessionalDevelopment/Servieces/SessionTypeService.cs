﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;
using AloFramework.Core.Utilities;
using AloFramework.Auth.Utilities;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public class SessionTypeService : ISessionTypeService
    {

        #region Properties

        private ISessionTypeRepository sessionTypeRepo;

        #endregion


        #region Constructor

        public SessionTypeService(ISessionTypeRepository sessionTypeRepo)
        {
            this.sessionTypeRepo = sessionTypeRepo;
        }

        #endregion


        #region Methods

        public List<SessionType> GetAllSessionTypes(SessionTypeListViewModel sessionTypeListViewModel)
        {
            var sessionTypes = new List<SessionType>();
            var sessionTypeFilter = sessionTypeListViewModel.SessionTypeFilter;
            try
            {
                if (sessionTypeFilter == null)
                    sessionTypeFilter = new SessionTypeFilterModel();
                sessionTypeListViewModel.SessionTypeFilter = sessionTypeFilter;
                sessionTypes = this.sessionTypeRepo.GetAllSessionTypes(sessionTypeListViewModel);
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in retriving SessionTypes list, due to " + ex);
            }
            return sessionTypes;
        }

        public List<SessionType> GetSessionTypes()
        {
            var sessionTypes = new List<SessionType>();
            try
            {
                sessionTypes = this.sessionTypeRepo.GetSessionTypes();
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in retriving SessionTypes list, due to " + ex);
            }
            return sessionTypes;
        }

        public SessionType GetSessionType(long sessionTypeID)
        {
            return this.sessionTypeRepo.GetSessionType(sessionTypeID);
        }

        public int SaveSessionType(SessionType sessionType)
        {
            try
            {
                LogUtil.Info("Trying to save SessionType, SessionType Data = " + Newtonsoft.Json.JsonConvert.SerializeObject(sessionType));
                if (sessionType.SessionTypeID > 0)
                {
                    return this.sessionTypeRepo.Edit(sessionType);
                }
                else
                {
                    return this.sessionTypeRepo.SaveSessionType(sessionType);
                }

            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in saving SessionType, due to " + ex);
            }
            return 0;
        }

        public void Delete(long? sessionTypeID) {
            this.sessionTypeRepo.Delete(sessionTypeID);
        }
        #endregion
    }
}
