﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public interface IScopeService
    {
        
        List<Scope> GetScopes();
        



    }
}
