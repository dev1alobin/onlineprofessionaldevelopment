﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public interface ISessionTypeService
    {

        List<SessionType> GetAllSessionTypes(SessionTypeListViewModel sessionTypeListViewModel);

        SessionType GetSessionType(long sessionTypeID);

        int SaveSessionType(SessionType sessionType);

        void Delete(long? sessionTypeID);

        List<SessionType> GetSessionTypes();
    }
}
