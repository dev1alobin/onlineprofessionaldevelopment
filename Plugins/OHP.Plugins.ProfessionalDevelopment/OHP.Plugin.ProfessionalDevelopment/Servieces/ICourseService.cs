﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public interface ICourseService
    {

        List<Course> GetAllCourses(CourseListViewModel courseListViewModel);

        Course GetCourse(long courseID);

        int SaveCourse(Course course);

        void Delete(long? courseID);

        List<Course> GetCourses();



    }
}
