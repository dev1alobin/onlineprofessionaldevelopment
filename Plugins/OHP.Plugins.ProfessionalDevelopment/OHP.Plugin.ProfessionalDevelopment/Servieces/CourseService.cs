﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Repositories;
using AloFramework.Core.Utilities;
using AloFramework.Auth.Utilities;

namespace OPD.Plugin.ProfessionalDevelopment.Services
{
    public class CourseService : ICourseService
    {

        #region Properties

        private ICourseRepository courseRepo;

        #endregion


        #region Constructor

        public CourseService(ICourseRepository courseRepo)
        {
            this.courseRepo = courseRepo;
        }

        #endregion


        #region Methods

        public List<Course> GetAllCourses(CourseListViewModel courseListViewModel)
        {
            var courses = new List<Course>();
            var courseFilter = courseListViewModel.CourseFilter;
            try
            {
                if (courseFilter == null)
                    courseFilter = new CourseFilterModel();
                courseListViewModel.CourseFilter = courseFilter;
                courses = this.courseRepo.GetAllCourses(courseListViewModel);
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in retriving Courses list, due to " + ex);
            }
            return courses;
        }

        public List<Course> GetCourses()
        {
            var courses = new List<Course>();
            try
            {
              
                courses = this.courseRepo.GetCourses();
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in retriving Courses list, due to " + ex);
            }
            return courses;
        }

        public Course GetCourse(long courseID)
        {
            return this.courseRepo.GetCourse(courseID);
        }

        public int SaveCourse(Course course)
        {
            try
            {
                LogUtil.Info("Trying to save Course, Course Data = " + Newtonsoft.Json.JsonConvert.SerializeObject(course));
                if (course.CourseID > 0)
                {
                    return this.courseRepo.Edit(course);
                }
                else
                {
                    return this.courseRepo.SaveCourse(course);
                }

            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in saving Course, due to " + ex);
            }
            return 0;
        }

        public void Delete(long? courseID) {
            this.courseRepo.Delete(courseID);
        }
        #endregion
    }
}
