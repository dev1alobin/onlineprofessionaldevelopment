﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public interface ISessionTypeRepository
    {

        List<SessionType> GetAllSessionTypes(SessionTypeListViewModel sessionTypeListViewModel);

        SessionType GetSessionType(long sessionTypeID);

        int SaveSessionType(SessionType sessionType);

        int Edit(SessionType sessionType);

        void Delete(long? sessionTypeID);

        List<SessionType> GetSessionTypes();


    }
}
