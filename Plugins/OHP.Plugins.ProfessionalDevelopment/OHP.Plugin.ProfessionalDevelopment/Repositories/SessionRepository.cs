﻿using AloFramework.DatabaseContext.Repositories;
using AloFramework.DatabaseContext.Models.PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using AloFramework.Auth.Utilities;
using AloFramework.Core.Utilities;
using System.Web;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public class SessionRepository : Repository, ISessionRepository
    {

        #region Methods

        private const string SESSION_LOGGED_USERID = "LoggedUserID";

        public List<Session> GetAllSessions(SessionListViewModel sessionListViewModel)
        {
            var sessionFilter = sessionListViewModel.SessionFilter;
            var query = new Sql("Select * from Sessions where IsActive = @0",1);
            if (sessionListViewModel.SessionFilter.SortParam != null)
            {
              query.OrderBy(sessionFilter.SortParam + " " + sessionFilter.SortOrder);
            }
            return this.dbContext.Fetch<Session>(query);
        }

        public Session GetSession(long sessionID)
        {

            var query = new Sql(@"Select Sessions.*, Courses.CourseName, Courses.CourseID
                                    , SessionTypes.SessionTypeName
                                    , SessionTypes.SessionTypeID
                                    , SessionScopes.*
                                    from Sessions");

            query.LeftJoin("Courses").On("Courses.CourseID = Sessions.CourseID");
            query.LeftJoin("SessionTypes").On("SessionTypes.SessionTypeID = Sessions.SessionTypeID");
            query.LeftJoin("SessionScopes").On("SessionScopes.SessionID = Sessions.SessionID");
            query.Where("Sessions.SessionID = @0", sessionID);
            //var query = new Sql("Select * from Sessions where SessionID = @0", sessionID);

            return this.dbContext.FetchOneToMany<Session,SessionScope>(session => session.SessionID, query).FirstOrDefault();
        }

        /// <summary>
        /// if return 1 => saved sucessfully
        /// retutn = 0 => in case of error
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        public int SaveSession(Session session)
        {
            try
            {
                /* var sessionExistsQuery = new Sql("Select * from Sessions where SessionName = @0 and SessionID <> @1 and IsActive= @2"
                                                             , session.SessionName
                                                             , session.SessionID
                                                             ,1);
                 var sessionExists = this.dbContext.Fetch<Session>(sessionExistsQuery).FirstOrDefault();
                 if (sessionExists != null)
                 {
                     LogUtil.Info("Session already exists, [SessionName = " + session.SessionName + ", SessionID = " + session.SessionID + "] ");
                     return -1;
                 }
                 else
                 {*/
                session.CreatedAt = DateTime.Now;
                session.UpdatedAt = DateTime.Now;
                session.IsActive = 1;
                this.dbContext.Save(session);
                using (var tx = this.dbContext.GetTransaction())
                {
                    LogUtil.Info("inserting SessionScopes");
                    foreach (var sessionScope in session.SessionScopes)
                    {
                        sessionScope.SessionID = session.SessionID;
                        this.dbContext.Insert(sessionScope);

                    }
                    tx.Complete();
                }
                LogUtil.Info("Session saved successfully");

                //}
                return 1;
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in saving Session, due to " + ex);
            }
            return 0;
        }

        public int Edit(Session session) {
            session.CreatedAt = DateTime.Now;
            session.UpdatedAt = DateTime.Now;
            session.IsActive = 1;
            return this.dbContext.Update(session);
        }

        public void Delete(long? sessionID) {
            var query = new Sql("set IsActive = @0 where SessionID = @1",0, sessionID);
            this.dbContext.Update<Session>(query);
        }
        #endregion
    }
}
