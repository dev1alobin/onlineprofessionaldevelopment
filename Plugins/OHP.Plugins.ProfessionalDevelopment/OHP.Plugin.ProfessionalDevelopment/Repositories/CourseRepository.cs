﻿using AloFramework.DatabaseContext.Repositories;
using AloFramework.DatabaseContext.Models.PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using AloFramework.Auth.Utilities;
using AloFramework.Core.Utilities;
using System.Web;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public class CourseRepository : Repository, ICourseRepository
    {

        #region Methods

        private const string SESSION_LOGGED_USERID = "LoggedUserID";

        public List<Course> GetAllCourses(CourseListViewModel courseListViewModel)
        {
            var courseFilter = courseListViewModel.CourseFilter;
            var query = new Sql("Select * from Courses where IsActive = @0",1);
            if (courseListViewModel.CourseFilter.SortParam != null)
            {
               query.OrderBy(courseFilter.SortParam + " " + courseFilter.SortOrder);
            }
            return this.dbContext.Fetch<Course>(query);
        }

        public List<Course> GetCourses()
        {
            var query = new Sql("Select * from Courses where IsActive = @0", 1);
            return this.dbContext.Fetch<Course>(query);
        }

        public Course GetCourse(long courseID)
        {
            var query = new Sql("Select * from Courses where CourseID = @0", courseID);
            return this.dbContext.Fetch<Course>(query).FirstOrDefault();
        }

        /// <summary>
        /// if return 1 => saved sucessfully
        /// retutn = 0 => in case of error
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        public int SaveCourse(Course course)
        {
            try
            {
                var courseExistsQuery = new Sql("Select * from Courses where CourseName = @0 and CourseID <> @1 and IsActive= @2"
                                                            , course.CourseName
                                                            , course.CourseID
                                                            ,1);
                var courseExists = this.dbContext.Fetch<Course>(courseExistsQuery).FirstOrDefault();
                if (courseExists != null)
                {
                    LogUtil.Info("Course already exists, [CourseName = " + course.CourseName + ", CourseID = " + course.CourseID + "] ");
                    return -1;
                }
                else
                {
                    course.CreatedAt = DateTime.Now;
                    course.UpdatedAt = DateTime.Now;
                    course.IsActive = 1;
                  //  long LoggedUserID = Convert.ToInt64(Session[SESSION_LOGGED_USERID]);
                  //  course.UserID = LoggedUserID;
                    this.dbContext.Save(course);
                    LogUtil.Info("Course saved successfully");
                }
                return 1;
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in saving Course, due to " + ex);
            }
            return 0;
        }

        public int Edit(Course course) {
            course.CreatedAt = DateTime.Now;
            course.UpdatedAt = DateTime.Now;
            course.IsActive = 1;
            return this.dbContext.Update(course);
        }

        public void Delete(long? courseID) {
            var query = new Sql("set IsActive = @0 where CourseID = @1",0, courseID);
            this.dbContext.Update<Course>(query);
        }
        #endregion
    }
}
