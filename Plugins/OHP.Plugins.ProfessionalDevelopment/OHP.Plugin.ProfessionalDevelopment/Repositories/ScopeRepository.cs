﻿using AloFramework.DatabaseContext.Repositories;
using AloFramework.DatabaseContext.Models.PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using AloFramework.Auth.Utilities;
using AloFramework.Core.Utilities;
using System.Web;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public class ScopeRepository : Repository, IScopeRepository
    {

        #region Methods
          

        public List<Scope> GetScopes()
        {
            var query = new Sql("Select * from Scopes where IsActive = @0", 1);
            return this.dbContext.Fetch<Scope>(query);
        }
        
     
        #endregion
    }
}
