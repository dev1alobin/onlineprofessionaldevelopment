﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public interface IScopeRepository
    {
        

        List<Scope> GetScopes();


    }
}
