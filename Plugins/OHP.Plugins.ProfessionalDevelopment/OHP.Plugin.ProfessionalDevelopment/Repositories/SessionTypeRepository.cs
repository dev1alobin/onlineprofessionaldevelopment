﻿using AloFramework.DatabaseContext.Repositories;
using AloFramework.DatabaseContext.Models.PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using AloFramework.Auth.Utilities;
using AloFramework.Core.Utilities;
using System.Web;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public class SessionTypeRepository : Repository, ISessionTypeRepository
    {

        #region Methods

        private const string SESSION_LOGGED_USERID = "LoggedUserID";

        public List<SessionType> GetAllSessionTypes(SessionTypeListViewModel sessionTypeListViewModel)
        {
            var sessionTypeFilter = sessionTypeListViewModel.SessionTypeFilter;
            var query = new Sql("Select * from SessionTypes where IsActive = @0",1);
            if (sessionTypeListViewModel.SessionTypeFilter.SortParam != null)
            {
               query.OrderBy(sessionTypeFilter.SortParam + " " + sessionTypeFilter.SortOrder);
            }
            return this.dbContext.Fetch<SessionType>(query);
        }

        public List<SessionType> GetSessionTypes()
        {
            var query = new Sql("Select * from SessionTypes where IsActive = @0", 1);
            return this.dbContext.Fetch<SessionType>(query);
        }

        public SessionType GetSessionType(long sessionTypeID)
        {
            var query = new Sql("Select * from SessionTypes where SessionTypeID = @0", sessionTypeID);
            return this.dbContext.Fetch<SessionType>(query).FirstOrDefault();
        }

        /// <summary>
        /// if return 1 => saved sucessfully
        /// retutn = 0 => in case of error
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        public int SaveSessionType(SessionType sessionType)
        {
            try
            {
                var sessionTypeExistsQuery = new Sql("Select * from SessionTypes where SessionTypeName = @0 and SessionTypeID <> @1 and IsActive= @2"
                                                            , sessionType.SessionTypeName
                                                            , sessionType.SessionTypeID
                                                            ,1);
                var sessionTypeExists = this.dbContext.Fetch<SessionType>(sessionTypeExistsQuery).FirstOrDefault();
                if (sessionTypeExists != null)
                {
                    LogUtil.Info("SessionType already exists, [SessionTypeName = " + sessionType.SessionTypeName + ", SessionTypeID = " + sessionType.SessionTypeID + "] ");
                    return -1;
                }
                else
                {
                    sessionType.CreatedAt = DateTime.Now;
                    sessionType.UpdatedAt = DateTime.Now;
                    sessionType.IsActive = 1;
                  //  long LoggedUserID = Convert.ToInt64(Session[SESSION_LOGGED_USERID]);
                  //  sessionType.UserID = LoggedUserID;
                    this.dbContext.Save(sessionType);
                    LogUtil.Info("SessionType saved successfully");
                }
                return 1;
            }
            catch (Exception ex)
            {
                LogUtil.Error("Error in saving SessionType, due to " + ex);
            }
            return 0;
        }

        public int Edit(SessionType sessionType) {
            sessionType.CreatedAt = DateTime.Now;
            sessionType.UpdatedAt = DateTime.Now;
            sessionType.IsActive = 1;
            return this.dbContext.Update(sessionType);
        }

        public void Delete(long? sessionTypeID) {
            var query = new Sql("set IsActive = @0 where SessionTypeID = @1",0, sessionTypeID);
            this.dbContext.Update<SessionType>(query);
        }
        #endregion
    }
}
