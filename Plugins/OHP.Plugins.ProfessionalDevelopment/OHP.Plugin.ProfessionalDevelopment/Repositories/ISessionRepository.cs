﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public interface ISessionRepository
    {

        List<Session> GetAllSessions(SessionListViewModel sessionListViewModel);

        Session GetSession(long sessionID);

        int SaveSession(Session session);

        int Edit(Session session);

        void Delete(long? sessionID);
        

    }
}
