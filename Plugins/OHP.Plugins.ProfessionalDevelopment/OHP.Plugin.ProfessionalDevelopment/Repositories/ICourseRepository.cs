﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;

namespace OPD.Plugin.ProfessionalDevelopment.Repositories
{
    public interface ICourseRepository
    {

        List<Course> GetAllCourses(CourseListViewModel courseListViewModel);

        Course GetCourse(long courseID);

        int SaveCourse(Course course);

        int Edit(Course course);

        void Delete(long? courseID);

        List<Course> GetCourses();


    }
}
