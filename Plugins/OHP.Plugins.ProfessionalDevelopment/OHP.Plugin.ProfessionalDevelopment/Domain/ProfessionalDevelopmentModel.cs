﻿using AloFramework.DatabaseContext.Models.PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD.Plugin.ProfessionalDevelopment.Domain
{
    [TableName("Scopes")]
    [PrimaryKey("ScopeID")]
    public class Scope
    {
        public long ScopeID { get; set; }
        public string ScopeName { get; set; }
        public int OrderBy { get; set; }
        public int IsActive { get; set; }
    }

    [TableName("Terminals")]
    [PrimaryKey("TerminalID")]
    public class Terminal
    {
        public long TerminalID { get; set; }
        public string TerminalName { get; set; }
        public int OrderBy { get; set; }
        public int IsActive { get; set; }
    }

    [TableName("Courses")]
    [PrimaryKey("CourseID")]
    public class Course
    {
        public long CourseID { get; set; }
        public string CourseName { get; set; }
        public long UserID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int OrderBy { get; set; }
        public int IsActive { get; set; }

    }

    [TableName("SessionTypes")]
    [PrimaryKey("SessionTypeID")]
    public class SessionType
    {
        public long SessionTypeID { get; set; }
        public string SessionTypeName { get; set; }
        public long UserID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int OrderBy { get; set; }
        public int IsActive { get; set; }
    }

    [TableName("Sessions")]
    [PrimaryKey("SessionID")]
    public class Session
    {
        public long SessionID { get; set; }
        public string SessionName { get; set; }
        public long CourseID { get; set; }
        public long SessionTypeID { get; set; }
        public long UserID { get; set; }

        [ResultColumn]
        public List<SessionScope> SessionScopes { get; set; }

        [ResultColumn]
        public string CourseName { get; set; }

        [ResultColumn]
        public string SessionTypeName { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int IsActive { get; set; }
    }

    [TableName("SessionScopes")]
    [PrimaryKey("SessionScopeID")]
    public class SessionScope
    {
        public long SessionScopeID { get; set; }
        public long SessionID { get; set; }
        public long ScopeID { get; set; }
        public int IsMandatory { get; set; }
    }

    [TableName("SessionSchedules")]
    [PrimaryKey("SessionScheduleID")]
    public class SessionSchedule
    {
        public long SessionScheduleID { get; set; }
        public long SessionID { get; set; }
        public DateTime SessionStartDateTime { get; set; }
        public DateTime SessionEndDateTime { get; set; }
        public string Location { get; set; }
        public long TerminalID { get; set; }
        public long UserID { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int IsActive { get; set; }
    }

    [TableName("Locations")]
    [PrimaryKey("LocationID")]
    public class Location
    {
        public long LocationID { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public int OrderBy { get; set; }
        public int IsActive { get; set; }
    }

    [TableName("StaffTypes")]
    [PrimaryKey("StaffTypeID")]
    public class StaffType
    {
        public long StaffTypeID { get; set; }
        public string StaffTypeName { get; set; }
    }
    
    [TableName("Departments")]
    [PrimaryKey("DepartmentID")]
    public class Department
    {
        public long DepartmentID { get; set; }
        public string DepartmentName { get; set; }
    }

    [TableName("Staffs")]
    [PrimaryKey("StaffID")]
    public class Staff
    {
        public long StaffID { get; set; }
        public long ScopeID { get; set; }
        public string StaffNumber { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string StaffType { get; set; }
        public string Department { get; set; }
        public int IsActive { get; set; }
    }

    [TableName("SignInOutEvents")]
    [PrimaryKey("SignInOutEventID")]
    public class SignInOutEvent
    {
        public long SignInOutEventID { get; set; }
        public long SessionScheduleID { get; set; }
        public long StaffID { get; set; }
        public DateTime SignedInDateTime { get; set; }
        public DateTime SignedOutDateTime { get; set; }
    }

    
}
