﻿using AloFramework.Core.Container;
using Microsoft.Practices.Unity;
using OPD.Plugin.ProfessionalDevelopment.Services;
using OPD.Plugin.ProfessionalDevelopment.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OPD.Plugin.ProfessionalDevelopment.Container
{
    public class ProfessionalDevelopmentDependencyResolver : DependencyResolver
    {
        public override void Register(IUnityContainer container)
        {
            container.RegisterType<ICourseService, CourseService>();
            container.RegisterType<ICourseRepository, CourseRepository>();
            container.RegisterType<ISessionTypeService,SessionTypeService>();
            container.RegisterType<ISessionTypeRepository, SessionTypeRepository>();
            container.RegisterType<ISessionService, SessionService>();
            container.RegisterType<ISessionRepository, SessionRepository>();
            container.RegisterType<IScopeService, ScopeService>();
            container.RegisterType<IScopeRepository, ScopeRepository>();
        }
    }
}
