﻿using OPD.Plugin.ProfessionalDevelopment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD.Plugin.ProfessionalDevelopment.Presentation.Models
{
    public class CourseListViewModel
    {
        public List<Course> Courses { get; set; }
        public CourseFilterModel CourseFilter { get; set; }
    }

    public class CourseEditViewModel
    {
        public Course Course { get; set; }
    }

 
    public class CourseFilterModel
    {
        public string SortOrder { get; set; }
        public string SortParam { get; set; }
    }
}
