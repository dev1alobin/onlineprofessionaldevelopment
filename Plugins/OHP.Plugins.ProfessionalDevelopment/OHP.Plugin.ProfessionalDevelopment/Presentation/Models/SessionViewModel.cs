﻿using OPD.Plugin.ProfessionalDevelopment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD.Plugin.ProfessionalDevelopment.Presentation.Models
{
    public class SessionListViewModel
    {
        public List<Session> Sessions { get; set; }
        public SessionFilterModel SessionFilter { get; set; }
    }

    public class SessionEditViewModel
    {
        public Session Session { get; set; }
        public List<SessionType> SessionTypes { get; set; }
        public List<Course> Courses { get; set; }
        public List<Scope> Scopes { get; set; }
        public List<SessionScope> SessionScopes { get; set; }
        //public long[] ScopeID { get; set; }
        //public long[] IsMandatory { get; set; }
    }


    public class SessionFilterModel
    {
        public string SortOrder { get; set; }
        public string SortParam { get; set; }
    }
}
