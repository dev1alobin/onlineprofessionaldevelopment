﻿using OPD.Plugin.ProfessionalDevelopment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD.Plugin.ProfessionalDevelopment.Presentation.Models
{
    public class SessionTypeListViewModel
    {
        public List<SessionType> SessionTypes { get; set; }
        public SessionTypeFilterModel SessionTypeFilter { get; set; }
    }

    public class SessionTypeEditViewModel
    {
        public SessionType SessionType { get; set; }
    }

 
    public class SessionTypeFilterModel
    {
        public string SortOrder { get; set; }
        public string SortParam { get; set; }
    }
}
