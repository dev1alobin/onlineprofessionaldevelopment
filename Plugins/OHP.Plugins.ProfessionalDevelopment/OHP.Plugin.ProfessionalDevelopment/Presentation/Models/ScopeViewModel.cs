﻿using OPD.Plugin.ProfessionalDevelopment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPD.Plugin.ProfessionalDevelopment.Presentation.Models
{
    public class ScopeListViewModel
    {
        public List<Scope> Scopes { get; set; }
        public ScopeFilterModel ScopeFilter { get; set; }
    }

    public class ScopeEditViewModel
    {
        public Scope Scope { get; set; }
    }


    public class ScopeFilterModel
    {
        public string SortOrder { get; set; }
        public string SortParam { get; set; }
    }
}
