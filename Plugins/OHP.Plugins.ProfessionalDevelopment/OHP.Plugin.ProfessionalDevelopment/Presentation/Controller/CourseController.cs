﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Services;
using AloFramework.Core.Utilities;


namespace OPD.Plugin.ProfessionalDevelopment.Presentation.Controllers
{
    public class CourseController : Controller
    {
        #region Properties
        private ICourseService courseService;
        private const string SESSION_LOGGED_USERID = "LoggedUserID";
        #endregion

        #region Constructor

        public CourseController(ICourseService courseService)
        {
            this.courseService = courseService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Use to display the List of Courses
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult Index(CourseListViewModel courseListViewModel)
        {
            courseListViewModel.Courses = this.courseService.GetAllCourses(courseListViewModel);
            return View(courseListViewModel);
        }

        /// <summary>
        /// Used to return Course create screen
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var courseEditViewModel = new CourseEditViewModel
            {
                Course = new Course()
            };
            return View(courseEditViewModel);
        }

        /// <summary>
        /// Used to Create and Update Course
        /// </summary>
        /// <param name="courseEditViewModel"></param>
        /// <returns></returns>
        public JsonResult SaveCourse(CourseEditViewModel courseEditViewModel)
        {
            try { 
            long LoggedUserID = Convert.ToInt64(Session[SESSION_LOGGED_USERID]);
            courseEditViewModel.Course.UserID = LoggedUserID;
            int result = this.courseService.SaveCourse(courseEditViewModel.Course);
            var errorMSG = "";
            if (result == 1)
                return Json("{\"Result\":\"Success\"}", JsonRequestBehavior.AllowGet);
            else if (result == -1)
                errorMSG = "Course Name already exists, try another name.";
            else
                errorMSG = "Problem in saving Course, please try again.";
            return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + errorMSG + "\"}", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e){
                LogUtil.Info("Error in Save Course " + e.Message);
            }
            return null;
        }

        /// <summary>
        /// return Course for given CourseID
        /// </summary>
        /// <param name="courseID"></param>
        /// <returns></returns>
        public ActionResult Edit(long courseID)
        {
            try
            {
               // var errorMSG = "";
                var courseEditViewModel = new CourseEditViewModel();
                var course = new Course
                {
                    CourseID = courseID
                };
                courseEditViewModel.Course = course;
                courseEditViewModel.Course = this.courseService.GetCourse(courseID);
                LogUtil.Info("Edit Course Output CourseName =" + courseEditViewModel.Course.CourseName);
                return View("Create", courseEditViewModel);
                //  return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + courseEditViewModel + "\"}", JsonRequestBehavior.AllowGet);
                //  return Json(courseEditViewModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {
                LogUtil.Info("Error in Edit Course due to "+ e.Message);
            }
            return null;
        }

        /// <summary>
        /// return Delete for given CourseID
        /// </summary>
        /// <param name="courseID"></param>
        /// <returns></returns>
        public JsonResult Delete(long? courseID)
        {
            try
            {
                this.courseService.Delete(courseID);
                return Json("{\"Result\":\"Success\"}", JsonRequestBehavior.AllowGet);
                //   return View("Index");
                //  return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + courseEditViewModel + "\"}", JsonRequestBehavior.AllowGet);
                //  return Json(courseEditViewModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                LogUtil.Info("Error in Delete Course due to " + e.Message);
            }
            return null;
        }
        /*public ActionResult Delete(long? courseID)
        {
            try
            {
                LogUtil.Info("Inside Delete CourseID = " + courseID);
                this.courseService.Delete(courseID);
               // return Json("{\"Result\":\"Success\"}", JsonRequestBehavior.AllowGet);
                 return RedirectToAction("Index");
                //  return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + courseEditViewModel + "\"}", JsonRequestBehavior.AllowGet);
                //  return Json(courseEditViewModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                LogUtil.Info("Error in Delete Course due to " + e.Message);
            }
            return null;
        }*/
        #endregion
    }
}
