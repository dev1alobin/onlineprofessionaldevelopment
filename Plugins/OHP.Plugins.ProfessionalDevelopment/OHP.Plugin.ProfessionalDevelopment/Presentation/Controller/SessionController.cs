﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Services;
using AloFramework.Core.Utilities;


namespace OPD.Plugin.ProfessionalDevelopment.Presentation.Controllers
{
    public class SessionController : Controller
    {
        #region Properties
        private ISessionService sessionService;
        private ICourseService courseService;
        private ISessionTypeService sessionTypeService;
        private IScopeService scopeService;
        private const string SESSION_LOGGED_USERID = "LoggedUserID";
        #endregion

        #region Constructor

        public SessionController(ISessionService sessionService
            , ICourseService courseService
            , ISessionTypeService sessionTypeService
            , IScopeService scopeService
            )
        {
            this.sessionService = sessionService;
            this.courseService = courseService;
            this.sessionTypeService = sessionTypeService;
            this.scopeService = scopeService;

        }

        #endregion

        #region Methods

        /// <summary>
        /// Use to display the List of Sessions
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult Index(SessionListViewModel sessionListViewModel)
        {
            sessionListViewModel.Sessions = this.sessionService.GetAllSessions(sessionListViewModel);
            return View(sessionListViewModel);
        }

        /// <summary>
        /// Used to return Session create screen
        /// </summary>
        /// <returns></returns>
        public ActionResult Create(SessionEditViewModel sessionEditViewModel)
        {
            var courses = this.courseService.GetCourses();
            var sessionTypes = this.sessionTypeService.GetSessionTypes();
            var scopes = this.scopeService.GetScopes();
            sessionEditViewModel = new SessionEditViewModel
            {
                Session = new Session(),
                Courses =  courses,
                SessionTypes = sessionTypes,
                Scopes = scopes
            };
            return View(sessionEditViewModel);
        }

        /// <summary>
        /// Used to Create and Update Session
        /// </summary>
        /// <param name="sessionEditViewModel"></param>
        /// <returns></returns>
        public JsonResult SaveSession(SessionEditViewModel sessionEditViewModel)
        {
            try {
            LogUtil.Info("Trying to save session, ScopeIDs = " + Newtonsoft.Json.JsonConvert.SerializeObject(sessionEditViewModel.Session.SessionScopes));
           // LogUtil.Info("Trying to save session, IsMandatory = " + Newtonsoft.Json.JsonConvert.SerializeObject(sessionEditViewModel.IsMandatory));
            long LoggedUserID = Convert.ToInt64(Session[SESSION_LOGGED_USERID]);
            sessionEditViewModel.Session.UserID = LoggedUserID;
          //  int result = this.sessionService.SaveSession(sessionEditViewModel.Session,sessionEditViewModel.ScopeID,sessionEditViewModel.IsMandatory);
            int result = this.sessionService.SaveSession(sessionEditViewModel.Session);
            var errorMSG = "";
            if (result == 1)
                return Json("{\"Result\":\"Success\"}", JsonRequestBehavior.AllowGet);
            else if (result == -1)
                errorMSG = "Session Name already exists, try another name.";
            else
                errorMSG = "Problem in saving Session, please try again.";
            return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + errorMSG + "\"}", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e){
                LogUtil.Info("Error in Save Session " + e.Message);
            }
            return null;
        }

        /// <summary>
        /// return Session for given SessionID
        /// </summary>
        /// <param name="SessionID"></param>
        /// <returns></returns>
        public ActionResult Edit(long sessionID)
        {
            try
            {
               // var errorMSG = "";
                var sessionEditViewModel = new SessionEditViewModel();
                var session = new Session
                {
                    SessionID = sessionID
                };
                sessionEditViewModel.Session = session;
                sessionEditViewModel.Session = this.sessionService.GetSession(sessionID);
                var courses = this.courseService.GetCourses();
                var sessionTypes = this.sessionTypeService.GetSessionTypes();
                var scopes = this.scopeService.GetScopes();
                sessionEditViewModel = new SessionEditViewModel
                {
                    Session = sessionEditViewModel.Session,
                    Courses = courses,
                    SessionTypes = sessionTypes,
                    Scopes = scopes
                };
                LogUtil.Info("Edit Session Output SessionName =" + sessionEditViewModel.Session.SessionName);
                return View("Create", sessionEditViewModel);
                //  return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + sessionEditViewModel + "\"}", JsonRequestBehavior.AllowGet);
                //  return Json(sessionEditViewModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {
                LogUtil.Info("Error in Edit Session due to "+ e.Message);
            }
            return null;
        }

        /// <summary>
        /// return Delete for given SessionID
        /// </summary>
        /// <param name="sessionID"></param>
        /// <returns></returns>
        public JsonResult Delete(long? sessionID)
        {
            try
            {
                this.sessionService.Delete(sessionID);
                return Json("{\"Result\":\"Success\"}", JsonRequestBehavior.AllowGet);
                //   return View("Index");
                //  return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + sessionEditViewModel + "\"}", JsonRequestBehavior.AllowGet);
                //  return Json(sessionEditViewModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                LogUtil.Info("Error in Delete Session due to " + e.Message);
            }
            return null;
        }
       
        
        #endregion
    }
}
