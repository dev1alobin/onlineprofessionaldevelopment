﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using OPD.Plugin.ProfessionalDevelopment.Presentation.Models;
using OPD.Plugin.ProfessionalDevelopment.Domain;
using OPD.Plugin.ProfessionalDevelopment.Services;
using AloFramework.Core.Utilities;


namespace OPD.Plugin.ProfessionalDevelopment.Presentation.Controllers
{
    public class SessionTypeController : Controller
    {
        #region Properties
        private ISessionTypeService sessionTypeService;
        private const string SESSION_LOGGED_USERID = "LoggedUserID";
        #endregion

        #region Constructor

        public SessionTypeController(ISessionTypeService sessionTypeService)
        {
            this.sessionTypeService = sessionTypeService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Use to display the List of SessionTypes
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public ActionResult Index(SessionTypeListViewModel sessionTypeListViewModel)
        {
            sessionTypeListViewModel.SessionTypes = this.sessionTypeService.GetAllSessionTypes(sessionTypeListViewModel);
            return View(sessionTypeListViewModel);
        }

        /// <summary>
        /// Used to return SessionType create screen
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var sessionTypeEditViewModel = new SessionTypeEditViewModel
            {
                SessionType = new SessionType()
            };
            return View(sessionTypeEditViewModel);
        }

        /// <summary>
        /// Used to Create and Update SessionType
        /// </summary>
        /// <param name="sessionTypeEditViewModel"></param>
        /// <returns></returns>
        public JsonResult SaveSessionType(SessionTypeEditViewModel sessionTypeEditViewModel)
        {
            try { 
            long LoggedUserID = Convert.ToInt64(Session[SESSION_LOGGED_USERID]);
            sessionTypeEditViewModel.SessionType.UserID = LoggedUserID;
            int result = this.sessionTypeService.SaveSessionType(sessionTypeEditViewModel.SessionType);
            var errorMSG = "";
            if (result == 1)
                return Json("{\"Result\":\"Success\"}", JsonRequestBehavior.AllowGet);
            else if (result == -1)
                errorMSG = "SessionType Name already exists, try another name.";
            else
                errorMSG = "Problem in saving SessionType, please try again.";
            return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + errorMSG + "\"}", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e){
                LogUtil.Info("Error in Save SessionType " + e.Message);
            }
            return null;
        }

        /// <summary>
        /// return SessionType for given SessionTypeID
        /// </summary>
        /// <param name="SessionTypeID"></param>
        /// <returns></returns>
        public ActionResult Edit(long sessionTypeID)
        {
            try
            {
               // var errorMSG = "";
                var sessionTypeEditViewModel = new SessionTypeEditViewModel();
                var sessionType = new SessionType
                {
                    SessionTypeID = sessionTypeID
                };
                sessionTypeEditViewModel.SessionType = sessionType;
                sessionTypeEditViewModel.SessionType = this.sessionTypeService.GetSessionType(sessionTypeID);
                LogUtil.Info("Edit SessionType Output SessionTypeName =" + sessionTypeEditViewModel.SessionType.SessionTypeName);
                return View("Create", sessionTypeEditViewModel);
                //  return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + sessionTypeEditViewModel + "\"}", JsonRequestBehavior.AllowGet);
                //  return Json(sessionTypeEditViewModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e) {
                LogUtil.Info("Error in Edit SessionType due to "+ e.Message);
            }
            return null;
        }

        /// <summary>
        /// return Delete for given SessionTypeID
        /// </summary>
        /// <param name="sessionTypeID"></param>
        /// <returns></returns>
        public JsonResult Delete(long? sessionTypeID)
        {
            try
            {
                this.sessionTypeService.Delete(sessionTypeID);
                return Json("{\"Result\":\"Success\"}", JsonRequestBehavior.AllowGet);
                //   return View("Index");
                //  return Json("{\"Result\":\"Failure\", \"ErrorMsg\" : \" " + sessionTypeEditViewModel + "\"}", JsonRequestBehavior.AllowGet);
                //  return Json(sessionTypeEditViewModel, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                LogUtil.Info("Error in Delete SessionType due to " + e.Message);
            }
            return null;
        }
        
        #endregion
    }
}
