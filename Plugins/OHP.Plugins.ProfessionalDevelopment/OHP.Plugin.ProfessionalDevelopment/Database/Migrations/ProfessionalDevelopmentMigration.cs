﻿using AloFramework.Migrator.Database;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.ProfessionalDevelopment.Database.Migrations
{
    [Migration(20161219144010)]
    public class ProfessionalDevelopment : AbstractAloMigration
    {
        public override void Down()
        {
            Delete.Table("Scopes");
            Delete.Table("Terminals");
            Delete.Table("Courses");
            Delete.Table("SessionTypes");
            Delete.Table("Sessions");
            Delete.Table("SessionTypes");
            Delete.Table("SessionScopes");
            Delete.Table("SessionSchedules");
            Delete.Table("Locations");
            Delete.Table("StaffTypes");
            Delete.Table("Departments");
            Delete.Table("Staffs");
            Delete.Table("SignInSignOuts");
        }

        public override void Up()
        {

            Create.Table("Scopes")
                    .WithIdColumn("ScopeID") 
                    .WithString("ScopeName")
                    .WithInteger("OrderBy").WithDefaultValue(1)
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("Terminals")
                    .WithIdColumn("TerminalID")
                    .WithString("TerminalName")
                    .WithInteger("OrderBy").WithDefaultValue(1)
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("Courses")
                    .WithIdColumn("CourseID")
                    .WithString("CourseName")
                    .WithForeignRef("User")
                    .WithDateTime("CreatedAt")
                    .WithDateTime("UpdatedAt")
                    .WithInteger("OrderBy").WithDefaultValue(1)
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("SessionTypes")
                    .WithIdColumn("SessionTypeID")
                    .WithString("SessionTypeName")
                    .WithForeignRef("User")
                    .WithDateTime("CreatedAt")
                    .WithDateTime("UpdatedAt")
                    .WithInteger("OrderBy").WithDefaultValue(1)
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("Sessions")
                    .WithIdColumn("SessionID")
                    .WithString("SessionName")
                    .WithForeignRef("Course")
                    .WithForeignRef("User")
                    .WithDateTime("CreatedAt")
                    .WithDateTime("UpdatedAt")
                    .WithForeignRef("SessionType")
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("SessionScopes")
                    .WithIdColumn("SessionScopeID")
                    .WithForeignRef("Session")
                    .WithForeignRef("Scope")
                    .WithTinyInt("IsMandatory").WithDefaultValue(1);

            Create.Table("Locations")
                    .WithIdColumn("LocationID")
                    .WithString("LocationCode").Nullable()
                    .WithString("LocationName")
                    .WithInteger("OrderBy").WithDefaultValue(1)
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("SessionSchedules")
                    .WithIdColumn("SessionScheduleID")
                    .WithForeignRef("Session")
                    .WithDateTime("SessionStartDateTime")
                    .WithDateTime("SessionEndDateTime")
                    .WithString("Location")
                    .WithColumn("TerminalID").AsInt64().Nullable()
                    .WithForeignRef("User")
                    .WithDateTime("CreatedAt")
                    .WithDateTime("UpdatedAt")
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("StaffTypes")
                    .WithIdColumn("StaffTypeID")
                    .WithString("StaffTypeName");

            Create.Table("Departments")
                    .WithIdColumn("DepartmentID")
                    .WithString("DepartmentName");
     
            Create.Table("Staffs")
                    .WithIdColumn("StaffID")
                    .WithForeignRef("Scope")
                    .WithString("StaffNumber")
                    .WithString("FirstName")
                    .WithString("Surname").Nullable()
                    .WithString("StaffType").Nullable()
                    .WithString("Department").Nullable()
                    .WithTinyInt("IsActive").WithDefaultValue(1);

            Create.Table("SignInSignOuts")
                    .WithIdColumn("SignInSignOutID")
                    .WithForeignRef("SessionSchedule")
                    .WithForeignRef("Staff")
                    .WithDateTime("SignedInDateTime").Nullable()
                    .WithDateTime("SignedOutDateTime").Nullable();
            
        }
    }
}
