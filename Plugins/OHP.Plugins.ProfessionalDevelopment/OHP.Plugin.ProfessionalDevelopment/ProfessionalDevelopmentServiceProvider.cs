﻿using AloFramework.Core.Container;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AloFramework.Core.Infrastructure;
using OPD.Plugin.ProfessionalDevelopment.Container;
namespace OPD.Plugin.ProfessionalDevelopment
{
    public class ProfessionalDevelopmentServiceProvider : ServiceProvider
    {
        public override void Boot(IApplicationContext appContext)
        {
            this.Bind(new ProfessionalDevelopmentDependencyResolver());

            base.Boot(appContext);
        }
    }
}
