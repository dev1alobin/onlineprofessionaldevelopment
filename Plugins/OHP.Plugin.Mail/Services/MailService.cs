﻿using OHP.Plugin.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel;
using System.Net.Mail;
using System.Net;
using AloFramework.Core.Utilities;

namespace OHP.Plugin.Mail.Services
{
    public class MailService : IMailService
    {
        private bool mailSent = false;
        private ISettingService settingService;
        private const string SESSION_LOGGED_USERID = "LoggedUserID"
            , SESSION_LOGGED_USER_TYPE = "LoggedUserType"
            , EMAIL_SETTINGS_FILTER = "EMAIL_"
            , S_EMAIL_NOTIFICATION = EMAIL_SETTINGS_FILTER + "Email_Notification"
            , S_TEST_MAIL = EMAIL_SETTINGS_FILTER + "Test_Mail"
            , S_TEST_MAIL_ADDRESS = EMAIL_SETTINGS_FILTER + "Test_Mail_Address"
            , S_FROM_ADDRESS = EMAIL_SETTINGS_FILTER + "From_Address"
            , S_SMTP_HOST = EMAIL_SETTINGS_FILTER + "Smtp_Host"
            , S_SMTP_USERNAME = EMAIL_SETTINGS_FILTER + "Smtp_Username"
            , S_SMTP_PASSWORD = EMAIL_SETTINGS_FILTER + "Smtp_Password"
            , S_SMTP_PORT = EMAIL_SETTINGS_FILTER + "Smtp_Port"
            , S_MS_PAYMENT_RECEIPT = EMAIL_SETTINGS_FILTER + "MS_Payment_Receipt"
            , S_MC_PAYMENT_RECEIPT = EMAIL_SETTINGS_FILTER + "MC_Payment_Receipt";

        private string EMAIL_NOTIFICATION;
        private string TEST_MAIL;
        private string TEST_MAIL_ADDRESS;
        private string FROM_ADDRESS;
        private string SMTP_HOST;
        private string SMTP_USERNAME;
        private string SMTP_PASSWORD;
        private string SMTP_PORT;
        private bool enableSSL = false;
        private string MS_PAYMENT_RECEIPT;
        private string MC_PAYMENT_RECEIPT;

        private static MailService Instance = null;

        public MailService(ISettingService settingService)
        {
            try
            {
                this.settingService = settingService;
                var emailSettings = this.settingService.GetOption(EMAIL_SETTINGS_FILTER);

                this.EMAIL_NOTIFICATION = this.settingService.GetOptionValue(S_EMAIL_NOTIFICATION);
                this.TEST_MAIL = this.settingService.GetOptionValue(S_TEST_MAIL);
                this.TEST_MAIL_ADDRESS = this.settingService.GetOptionValue(S_TEST_MAIL_ADDRESS);
                this.FROM_ADDRESS = this.settingService.GetOptionValue(S_FROM_ADDRESS);
                this.SMTP_HOST = this.settingService.GetOptionValue(S_SMTP_HOST);
                this.SMTP_USERNAME = this.settingService.GetOptionValue(S_SMTP_USERNAME);
                this.SMTP_PASSWORD = this.settingService.GetOptionValue(S_SMTP_PASSWORD);
                this.SMTP_PORT = this.settingService.GetOptionValue(S_SMTP_PORT);

                this.MS_PAYMENT_RECEIPT = this.settingService.GetOptionValue(S_MS_PAYMENT_RECEIPT);
                this.MC_PAYMENT_RECEIPT = this.settingService.GetOptionValue(S_MC_PAYMENT_RECEIPT);

                string enableSSLValue = System.Configuration.ConfigurationManager.AppSettings["EnableSSLForEmail"];
                LogUtil.Info("EnableSSLForEmail value = " + enableSSLValue);
                if (!string.IsNullOrEmpty(enableSSLValue)
                    && enableSSLValue.Equals("Y"))
                {
                    enableSSL = true;
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error("Initializing MailSender failed, due to " + ex);
            }
        }

        private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                LogUtil.Info("[" + token + "] Send canceled.");
            }
            if (e.Error != null)
            {
                LogUtil.Error("[" + token + "] " + e.Error.ToString());
            }
            else
            {
                LogUtil.Info("Message sent.");
            }
            mailSent = true;
        }


        public void SendMail(string ToAddress
            , string CC
            , string BCC
            , string Subject
            , string Body
            , IEnumerable<HttpPostedFileBase> attachmentFiles = null
            , bool isAsync = false)
        {
            var smtpSendThread = new Thread(() => SendMailThread(ToAddress, CC, BCC, Subject, Body, attachmentFiles, isAsync));
            smtpSendThread.Start();
        }

        public void SendMailThread(string ToAddress
            , string CC
            , string BCC
            , string Subject
            , string Body
            , IEnumerable<HttpPostedFileBase> attachmentFiles = null
            , bool isAsync = false)
        {
            try
            {
                LogUtil.Info("Email To " + ToAddress);
                if (EMAIL_NOTIFICATION.Equals("Yes"))
                {
                    if (String.IsNullOrEmpty(ToAddress))
                    {
                        ToAddress = FROM_ADDRESS;
                    }
                    LogUtil.Info("***  Email Notification Enabled  ***");
                    if (TEST_MAIL.Equals("Yes"))
                    {
                        LogUtil.Info("*** Test Email Enabled  ***");
                        ToAddress = TEST_MAIL_ADDRESS;
                    }
                    using (MailMessage mail = new MailMessage(FROM_ADDRESS, ToAddress))// +"," + CC + "," + BCC))
                    {
                        if (!String.IsNullOrEmpty(BCC))
                        {
                            mail.Bcc.Add(BCC);
                        }
                        mail.Subject = Subject;
                        mail.Body = Body;
                        mail.IsBodyHtml = true;

                        if (attachmentFiles != null)
                        {
                            foreach (var attachmentFile in attachmentFiles)
                            {
                                if (attachmentFile != null)
                                {
                                    System.Net.Mail.Attachment attachment;
                                    attachment = new System.Net.Mail.Attachment(attachmentFile.InputStream, attachmentFile.FileName);
                                    mail.Attachments.Add(attachment);
                                }
                            }
                        }

                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = SMTP_HOST;
                        smtp.EnableSsl = enableSSL;
                        NetworkCredential networkCredential = new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = networkCredential;
                        smtp.Port = Int32.Parse(SMTP_PORT);

                        if (isAsync)
                        {
                            Object state = mail;
                            smtp.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                            try
                            {
                                smtp.SendAsync(mail, mail);
                            }
                            catch (Exception ex)
                            {
                                LogUtil.Error("Mail sending error, due to " + ex);
                            }
                        }
                        else
                        {
                            smtp.Send(mail);
                            LogUtil.Info("Mail sent successfully to " + ToAddress);
                        }

                    }
                }
                else
                {
                    LogUtil.Info("Email Notification NOT Enabled ");
                }
            }
            catch (Exception e)
            {
                LogUtil.Error("Problem in sending Email, due to " + e);
            }
        }
    }
}
