﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Web;

namespace OHP.Plugin.Mail.Services
{
    public interface IMailService
    {

        void SendMail(string ToAddress
            , string CC
            , string BCC
            , string Subject
            , string Body
            , IEnumerable<HttpPostedFileBase> attachmentFiles = null
            , bool isAsync = false);

    }
}