﻿using AloFramework.Core.Container;
using AloFramework.Core.Infrastructure;
using OHP.Plugin.Mail.Container;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Mail
{
    public class MailServiceProvider : ServiceProvider
    {
        public override void Boot(IApplicationContext appContext)
        {
            this.Bind(new MailDependencyResolver());
            base.Boot(appContext);
        }
    }
}
