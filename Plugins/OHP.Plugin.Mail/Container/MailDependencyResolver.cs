﻿using AloFramework.Core.Container;
using Microsoft.Practices.Unity;
using OHP.Plugin.Mail.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Mail.Container
{
    public class MailDependencyResolver : DependencyResolver
    {
        public override void Register(IUnityContainer container)
        {
            container.RegisterType<IMailService, MailService>();
        }
    }
}
