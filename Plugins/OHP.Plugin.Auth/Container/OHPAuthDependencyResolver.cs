﻿using AloFramework.Core.Container;
using OHP.Plugin.Auth.Repositories;
using OHP.Plugin.Auth.Services;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OHP.Plugin.Auth.Container
{
    public class OHPAuthDependencyResolver : AloFramework.Core.Container.DependencyResolver
    {
        public override void Register(Microsoft.Practices.Unity.IUnityContainer container)
        {
            container.RegisterType<IAuthMailService, AuthMailService>();
            container.RegisterType<IOHPAuthRepository, OHPAuthRepository>();
            container.RegisterType<IOHPAuthService, OHPAuthService>();
        }
    }
}
