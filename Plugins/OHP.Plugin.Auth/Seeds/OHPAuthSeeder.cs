﻿using AloFramework.Migrator.Database;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AloFramework.Auth.Database.Seeds
{
    //[Migration(20151105153042)]
    public class OHPAuthSeeder : Migration
    {
        public override void Down()
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Update the database with dummy data
        /// </summary>
        public void Seed()
        {
            
            /// TODO: Add dummy users
            Insert.IntoTable("Users")
                .Row(new 
                {
                    Username = "parent1",
                    EmailAddress = "parent1@gmail.com",
                    Password = "timdamaal",
                    MobileNo = "11111111111",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    ReferenceNumber = "1ParentOne"
                });

            Insert.IntoTable("Users")
                .Row(new
                {
                    Username = "parent2",
                    EmailAddress = "parent2@gmail.com",
                    Password = "timdamaal",
                    MobileNo = "22222222222",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    ReferenceNumber = "2ParentTwo"
                });

            Insert.IntoTable("Users")
                .Row(new
                {
                    Username = "student1",
                    EmailAddress = "student1@gmail.com",
                    Password = "student1",
                    MobileNo = "111111111",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                });

            Insert.IntoTable("Users")
                .Row(new
                {
                    Username = "student2",
                    EmailAddress = "student2@gmail.com",
                    Password = "student2",
                    MobileNo = "22222222222",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                });

            Insert.IntoTable("Users")
                .Row(new
                {
                    Username = "student3",
                    EmailAddress = "student3@gmail.com",
                    Password = "student3",
                    MobileNo = "33333333333",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                });

            Insert.IntoTable("Users")
                .Row(new
                {
                    Username = "parent3",
                    EmailAddress = "parent3@gmail.com",
                    Password = "timdamaal",
                    MobileNo = "3333333333",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                    ReferenceNumber = "3ParentThree"
                });
        }

        public override void Up()
        {
            Seed();
        }
    }
}
