﻿using AloFramework.Core.Utilities;
using AloFramework.Migrator.Database;
using FluentMigrator;
using OHP.Plugin.Auth.Presentation.Models;
using OHP.Plugin.Auth.Services;
using OHP.Plugin.ParentRegistration.Domain;
using OHP.Plugin.StudentRegistration.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OHP.Plugin.Auth.Seeds
{
    [Migration(20151105153043)]
    public class UsersSeederImportCSV : AbstractAloMigration
    {
        #region Properties
        private IOHPUserSeederService ohpUserSeederService;
        #endregion

        /*#region Constructor
        public UsersSeederImportCSV(IOHPUserSeederService ohpUserSeederService)
        {
            this.ohpUserSeederService = ohpUserSeederService;
        }
        #endregion*/
            
        public override void Up()
        {
        /*try {
            var csvRows = File.ReadLines(HttpContext.Current.Server.MapPath(@"~/Storage/Seeds/Parents.csv"));
            int row = 0;
            foreach(var csvRow in csvRows)
            {
                if (row++ == 0)
                    continue;
                var csvRecord = csvRow.Split(',');
                if (this.ohpUserSeederService.AddUser(new OHPUserSeederModel
                    {
                        Username = csvRecord[0],
                        ForeignKey = csvRecord[1],
                        UserType = csvRecord[3],
                        Salutation = csvRecord[4],
                        FirstName = csvRecord[5],
                        Surname = csvRecord[6],
                        MobileNo = csvRecord[10],
                        EmailAddress = csvRecord[11],
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    })
                )
                {
                    this.ohpUserSeederService.AddParentInformation(new ParentInformation
                    {
                        ReferenceNumber = csvRecord[1],
                        FirstName = csvRecord[5],
                        Surname = csvRecord[6],
                        EmailAddress = csvRecord[11],
                        Username = csvRecord[0],
                        MobileContact = csvRecord[10],
                        Gender = (csvRecord[8] == "Father") ? "Male" : "Female"
                    });


                }// end of if



                if (this.ohpUserSeederService.AddStudentParentRelationship(new OHPStudentParentRelationshipModel
                    {
                        StudentUserID = csvRecord[2],
                        ForeignKey = csvRecord[1]
                    })
                )
                {

                }
            }


            var studentsDatas = File.ReadLines(HttpContext.Current.Server.MapPath(@"~/Storage/Seeds/Students.csv"));
            row = 0;
            foreach (var studentDataRow in studentsDatas)
            {
                if (row++ == 0)
                    continue;
                var studentRecord = studentDataRow.Split(',');

                this.ohpUserSeederService.AddStudentInformation(new StudentInformation {
                    ReferenceNumber = studentRecord[8],
                    FirstName = studentRecord[1],
                    Surname = studentRecord[2],
                    Gender = "Female",
                    StudentUserID = studentRecord[8],
                    EmailAddress = studentRecord[5],
                    SchoolYear = studentRecord[4],
                    DateOfBirth = DateTime.Now
                });
            }
        } catch(Exception ex)
        {
            LogUtil.Error("Error in uploading users csv records into database, due to "+ ex);
        }*/

    }
    }
}
