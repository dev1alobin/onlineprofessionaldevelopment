﻿using AloFramework.Migrator.Database;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Seeds
{
    [Migration(20160603181010)]
    public class ResetPasswordSettingsSeeder : AbstractAloMigration
    {
        public override void Up()
        {
            Insert.IntoTable("Settings")
                .Row(new
                {
                    OptionName = "EMAIL_Reset_Password_Subject",
                    OptionValue = "Reset Password Online Holiday Program"
                });

            Insert.IntoTable("Settings")
                .Row(new
                {
                    OptionName = "EMAIL_Reset_Password_Body",
                    OptionValue = @"<p>
                                        We received a request to reset the password associated with this email address. If you made this request, please follow the instructions below.
                                    </p>
                                    <p>
				                        If you did not  request to have your password reset, you can safely ignore the email. We assure that your account is safe.
                                    </p>
                                    <h3>Click the link below to reset your password:</h3>
                                    {0}"
                });
        }
    }
}

