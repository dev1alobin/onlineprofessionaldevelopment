﻿using AloFramework.Migrator.Database;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Seeds
{
    [Migration(20151216163510)]
    public class SupportEmailSettingsSeeder : AbstractAloMigration
    {
        public override void Up()
        {
            Insert.IntoTable("Settings")
                .Row(new
                {
                    OptionName = "ACCOUNT_SUPPORT_ICT_EMAIL_ADDRESS",
                    OptionValue = "test@gmail.com"
                });
        }
    }
}
