﻿using OHP.Plugin.Auth.Presentation.Models;
using OHP.Plugin.ParentRegistration.Domain;
using OHP.Plugin.StudentRegistration.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Services
{
    public interface IOHPUserSeederService
    {
        
        bool AddUser(OHPUserSeederModel ohpUserSeederModel);

        void AddParentInformation(ParentInformation parentInformationModel);

        bool AddStudentParentRelationship(OHPStudentParentRelationshipModel ohpstudentParentRelatioshipModel);

        void AddStudentInformation(StudentInformation studentInformationModel);

        void UserImporter();

    }
}
