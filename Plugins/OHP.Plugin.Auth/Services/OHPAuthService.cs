﻿using AloFramework.Auth.Utilities;
using OHP.Plugin.Auth.Utilities;
using OHP.Plugin.Auth.Presentation.Models;
using OHP.Plugin.Auth.Repositories;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AloFramework.Core.Utilities;
using System.Web.Security;

namespace OHP.Plugin.Auth.Services
{
    public class OHPAuthService : IOHPAuthService
    {
        #region Properties

        private IOHPAuthRepository ohpAuthRepo;
        private IAuthMailService authMailService;

        #endregion



        #region Constructor

        public OHPAuthService(IOHPAuthRepository ohpAuthRepo
                , IAuthMailService authMailService)
        {
            this.ohpAuthRepo = ohpAuthRepo;
            this.authMailService = authMailService;
        }

        #endregion



        #region IStudentInformationService Methods

        public bool ValidateUserModel(OHPUserModel ohpUserModel)
        {
            bool result = true;
            var langObject = LangUtil.GetFreshLangObject("OHPRegistration");
            if (String.IsNullOrEmpty(ohpUserModel.FirstName) || String.IsNullOrEmpty(ohpUserModel.FirstName.Trim()))
            {
                SessionUtil.Error((string)langObject.firstNameRequired);
                return false;
            }
            if (String.IsNullOrEmpty(ohpUserModel.Surname) || String.IsNullOrEmpty(ohpUserModel.Surname.Trim()))
            {
                SessionUtil.Error((string)langObject.surnameRequired);
                return false;
            }
            if (String.IsNullOrEmpty(ohpUserModel.Username) || String.IsNullOrEmpty(ohpUserModel.Username.Trim()))
            {
                SessionUtil.Error((string)langObject.usernameRequired);
                return false;
            }
            if (String.IsNullOrEmpty(ohpUserModel.EmailAddress) || String.IsNullOrEmpty(ohpUserModel.EmailAddress.Trim()))
            {
                SessionUtil.Error((string)langObject.emailAddressRequired);
                return false;
            }
            if (String.IsNullOrEmpty(ohpUserModel.Password) || String.IsNullOrEmpty(ohpUserModel.Password.Trim()))
            {
                SessionUtil.Error((string)langObject.passwordRequired);
                return false;
            }
            if (!ohpUserModel.Password.Equals(ohpUserModel.PasswordConfirmation))
            {
                SessionUtil.Error((string)langObject.passwordNotMatching);
                return false;
            }
            result = this.ValidateUserExists(ohpUserModel);
            return result;
        }

        public bool AuthenticateViaForeignKey(string foreignKey)
        {
            var user = this.ohpAuthRepo.AuthenticateViaForeignKey(foreignKey);
            if (user != null) {
                FormsAuthentication.SetAuthCookie(user.Username, false);

                SessionUtil.Set("ReferenceNumber", user.ReferenceNumber);
                SessionUtil.Set("LoggedUserDisplayName", user.DisplayName);
                return true;
            }
            return false;
        }

        public bool ResetPassword(string emailAddress)
        {
            string resetToken = Guid.NewGuid().ToString();
            try
            {
                this.ohpAuthRepo.ResetPassword(emailAddress, resetToken);
                this.authMailService.SendResetPasswordToken(emailAddress, resetToken);
                return true;
            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in generating token in reset password, due to " + ex);
                SessionUtil.Error("Problem in sending password reset link, please try again");
            }
            return false;
        }

        public bool ValidateResetPasswordModel(OHPResetPasswordModel ohpResetPasswordModel)
        {
            bool result = true;
            var langObject = LangUtil.GetFreshLangObject("OHPRegistration");
            if (String.IsNullOrEmpty(ohpResetPasswordModel.Password) || String.IsNullOrEmpty(ohpResetPasswordModel.Password.Trim()))
            {
                SessionUtil.Error((string)langObject.passwordRequired);
                return false;
            }
            if (!ohpResetPasswordModel.Password.Equals(ohpResetPasswordModel.PasswordConfirmation))
            {
                SessionUtil.Error((string)langObject.passwordNotMatching);
                return false;
            }
            return result;
        }

        public void DoResetPassword(OHPResetPasswordModel ohpResetPasswordModel)
        {
            this.ohpAuthRepo.DoResetPassword(ohpResetPasswordModel);
        }

        public bool ValidateUserExists(OHPUserModel ohpUserModel)
        {
            var user = this.ohpAuthRepo.CheckUserExists(ohpUserModel);
            if (user != null)
            {
                SessionUtil.Error("Username or EmailAddress already exists");
                return false;
            }
            return true;
        }

        public void UpdateUser(OHPUserModel ohpUserModel)
        {
            this.ohpAuthRepo.UpdateUser(ohpUserModel);
        }

        public OHPUserModel GetUserByUsername(string username)
        {
            return this.ohpAuthRepo.GetUserByUsername(username);
        }

        public OHPUserModel GetUserByForeignKey(string foreignKey)
        {
            return this.ohpAuthRepo.GetUserByForeignKey(foreignKey);
        }

        #endregion
    }
}
;