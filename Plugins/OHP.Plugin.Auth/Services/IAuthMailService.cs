﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Services
{
    public interface IAuthMailService
    {
        void SendResetPasswordToken(string emailAddress, string resetToken);
    }
}
