﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OHP.Plugin.Common.Services;
using OHP.Plugin.Mail.Services;

namespace OHP.Plugin.Auth.Services
{
    public class AuthMailService : IAuthMailService
    {

        #region Properties

        private const string SESSION_LOGGED_USERID = "LoggedUserID"
            , SESSION_LOGGED_USER_TYPE = "LoggedUserType"
            , EMAIL_SETTINGS_FILTER = "EMAIL_"
            , RESET_PASSWORD_SUBJECT = EMAIL_SETTINGS_FILTER + "Reset_Password_Subject"
            , RESET_PASSWORD_BODY = EMAIL_SETTINGS_FILTER + "Reset_Password_Body";

        private ISettingService settingService;
        private IMailService mailService;

        #endregion


        #region Constructoer

        public AuthMailService(ISettingService settingService
            , IMailService mailService)
        {
            this.settingService = settingService;
            this.mailService = mailService;
        }

        #endregion

        #region Methods

        public void SendResetPasswordToken(string emailAddress, string resetToken)
        {

            var emailSettings = this.settingService.GetOptions(EMAIL_SETTINGS_FILTER);
            var reset_password_subject = emailSettings.Where(e => e.OptionName.Equals(RESET_PASSWORD_SUBJECT)).FirstOrDefault().OptionValue;
            var reset_password_body = emailSettings.Where(e => e.OptionName.Equals(RESET_PASSWORD_BODY)).FirstOrDefault().OptionValue;


            string requestURI = HttpContext.Current.Request.Url.AbsoluteUri;
            requestURI = requestURI.Replace("ResetPassword", "PasswordReset?resetPasswordToken=" + resetToken);
            string url = @"<a href=" + requestURI + ">" + requestURI + "</a>";
            var data = String.Format(reset_password_body, url);

            /*var data = "<p>We received a request to reset the password associated with this email address. If you made this request, please follow the instructions below.</p>";
            data += @"<p>
				        If you did not  request to have your password reset, you can safely ignore the email. We assure that your account is safe.
                    </p>";
            data += @"<h3>Click the link below to reset your password:</h3>";
            data += @"<a href=" + requestURI + ">" + requestURI + "</a>";*/


            this.mailService.SendMail(emailAddress, null, null, reset_password_subject, data);

        }

        #endregion
    }
}
