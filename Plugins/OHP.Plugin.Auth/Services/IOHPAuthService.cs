﻿using OHP.Plugin.Auth.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Services
{
    public interface IOHPAuthService
    {
        bool ValidateUserModel(OHPUserModel ohpUserModel);

        bool ValidateUserExists(OHPUserModel ohpUserModel);

        bool AuthenticateViaForeignKey(string foreignKey);

        bool ResetPassword(string emailAddress);

        bool ValidateResetPasswordModel(OHPResetPasswordModel ohpResetPasswordModel);

        void DoResetPassword(OHPResetPasswordModel ohpResetPasswordModel);

        void UpdateUser(OHPUserModel ohpUserModel);

        OHPUserModel GetUserByUsername(string username);

        OHPUserModel GetUserByForeignKey(string foreignKey);

    }
}
