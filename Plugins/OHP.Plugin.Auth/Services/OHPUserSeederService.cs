﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OHP.Plugin.Auth.Presentation.Models;
using OHP.Plugin.Auth.Repositories;
using OHP.Plugin.ParentRegistration.Domain;
using OHP.Plugin.StudentRegistration.Domain;
using AloFramework.Core.Utilities;
using System.IO;
using System.Web;

namespace OHP.Plugin.Auth.Services
{
    public class OHPUserSeederService : IOHPUserSeederService
    {
        #region Properties

        private IOHPUserSeederRepository ohpUserSeederRepo;

        #endregion



        #region Constructor

        public OHPUserSeederService(IOHPUserSeederRepository ohpUserSeederRepo)
        {
            this.ohpUserSeederRepo = ohpUserSeederRepo;
        }

        #endregion



        #region IOHPUserSeederService Methods

        public bool AddUser(OHPUserSeederModel ohpUserSeederModel)
        {
            return this.ohpUserSeederRepo.AddUser(ohpUserSeederModel);
        }

        public void AddParentInformation(ParentInformation parentInformation)
        {
            this.ohpUserSeederRepo.AddParentInformation(parentInformation);
        }

        public bool AddStudentParentRelationship(OHPStudentParentRelationshipModel ohpStudentParentRelationshipModel)
        {
            return this.ohpUserSeederRepo.AddStudentParentRelationship(ohpStudentParentRelationshipModel);
        }

        public void AddStudentInformation(StudentInformation studentInformationModel)
        {
            this.ohpUserSeederRepo.AddStudentInformation(studentInformationModel);
        }



        public void UserImporter()
        {
           try
            {
                var csvRows = File.ReadLines(HttpContext.Current.Server.MapPath(@"~/Storage/Seeds/Parents.csv"));
                int row = 0;
                foreach (var csvRow in csvRows)
                {
                    if (row++ == 0)
                        continue;
                    var csvRecord = csvRow.Split(',');
                    if (this.AddUser(new OHPUserSeederModel
                    {
                        Username = csvRecord[0],
                        ForeignKey = csvRecord[1],
                        ReferenceNumber = csvRecord[1],
                        UserType = csvRecord[3],
                        Salutation = csvRecord[4],
                        FirstName = csvRecord[5],
                        Surname = csvRecord[6],
                        MobileNo = csvRecord[10],
                        EmailAddress = csvRecord[11],
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    })
                    )
                    {
                        this.AddParentInformation(new ParentInformation
                        {
                            ReferenceNumber = csvRecord[1],
                            FirstName = csvRecord[5],
                            Surname = csvRecord[6],
                            EmailAddress = csvRecord[11],
                            Username = csvRecord[0],
                            DateOfBirth = DateTime.Parse(csvRecord[16]), //Convert.ToDateTime(csvRecord[16] + " 00:00"),// DateTime.Parse(csvRecord[16] + " 00:00"),
                            MobileContact = csvRecord[10],
                            Gender = (csvRecord[8] == "Father") ? "Male" : "Female",
                            IsExistingParent = 1
                        });


                    }// end of if



                    if (this.AddStudentParentRelationship(new OHPStudentParentRelationshipModel
                    {
                        StudentUserID = csvRecord[2],
                        ParentUserID = csvRecord[0],
                        Relationship = csvRecord[8]
                    })
                    )
                    {

                    }
                }


                var studentsDatas = File.ReadLines(HttpContext.Current.Server.MapPath(@"~/Storage/Seeds/Students.csv"));
                row = 0;
                foreach (var studentDataRow in studentsDatas)
                {
                    if (row++ == 0)
                        continue;
                    var studentRecord = studentDataRow.Split(',');

                    this.AddUser(new OHPUserSeederModel
                    {
                        Username = studentRecord[0],
                        ForeignKey = studentRecord[8],
                        ReferenceNumber = studentRecord[8],
                        UserType = studentRecord[3],
                        Salutation = "",
                        FirstName = studentRecord[1],
                        Surname = studentRecord[2],
                        MobileNo = studentRecord[6],
                        EmailAddress = studentRecord[5],
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now
                    });

                    this.AddStudentInformation(new StudentInformation
                    {
                        ReferenceNumber = studentRecord[8],
                        FirstName = studentRecord[1],
                        Surname = studentRecord[2],
                        Gender = "Female",
                        StudentUserID = studentRecord[0],
                        ForeignKey = studentRecord[8],
                        EmailAddress = studentRecord[5],
                        SchoolYear = studentRecord[4],
                        DateOfBirth = DateTime.Parse(studentRecord[9])//Convert.ToDateTime(studentRecord[9] + " 00:00")// DateTime.Parse(studentRecord[9] + " 00:00")
                    });

                }
           }
           catch (Exception ex)
           {
               LogUtil.Error("Error in uploading users csv records into database, due to " + ex);
           }
        }

        #endregion
    }
}
