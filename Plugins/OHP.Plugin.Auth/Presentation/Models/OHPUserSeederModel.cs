﻿using AloFramework.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Presentation.Models
{
    public class OHPUserSeederModel : ResourceModel
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        public string ForeignKey { get; set; }
        public string UserType { get; set; }
        public string Salutation { get; set; }
        public string MobileNo { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
