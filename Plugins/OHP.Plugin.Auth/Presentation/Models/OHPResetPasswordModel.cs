﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Presentation.Models
{
    public class OHPResetPasswordModel
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        public string ResetPasswordToken { get; set; }
    }
}
