﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Presentation.Models
{
    public class OHPStudentParentRelationshipModel
    {
        public string StudentUserID { get; set; }
        public string ParentUserID { get; set; }
        //public string ForeignKey { get; set; }
        public string Relationship { get; set; }
    }
}
