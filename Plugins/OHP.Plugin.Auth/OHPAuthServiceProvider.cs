﻿using AloFramework.Core.Container;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AloFramework.Core.Infrastructure;
using OHP.Plugin.Auth.Container;

namespace OHP.Plugin.Auth
{
    public class OHPAuthServiceProvider : ServiceProvider
    {
        public override void Boot(IApplicationContext appContext)
        {
            this.Bind(new OHPAuthDependencyResolver());

            base.Boot(appContext);
        }
    }
}
