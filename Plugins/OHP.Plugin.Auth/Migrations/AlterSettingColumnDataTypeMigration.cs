﻿using AloFramework.Migrator.Database;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Migrations
{
    [Migration(20160603180510)]
    public class AlterSettingColumnDataTypeMigration : AbstractAloMigration
    {
        public override void Down()
        {
            Alter.Table("Settings").AlterColumn("OptionValue").AsString(1000).Nullable();
            base.Down();
        }

        public override void Up()
        {
            if (Schema.Table("Settings").Column("OptionValue").Exists())
            {
                Alter.Table("Settings").AlterColumn("OptionValue").AsString(1000).Nullable();
            }
        }
    }
}
