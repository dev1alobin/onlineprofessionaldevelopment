﻿using AloFramework.Migrator.Database;
using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Migrations
{
    [Migration(20151105143619)]
    public class AlterUsersTableWithReferenceNumber : AbstractAloMigration
    {
        public override void Down()
        {
            if (Schema.Table("Users").Column("ReferenceNumber").Exists())
            {
                Delete.Column("ReferenceNumber").FromTable("Users");
            }
            if (Schema.Table("Users").Column("IsAlreadyBooked").Exists())
            {
                Delete.Column("IsAlreadyBooked").FromTable("Users");
            }
            if (Schema.Table("Users").Column("LastBookedDate").Exists())
            {
                Delete.Column("LastBookedDate").FromTable("Users");
            }
            if (Schema.Table("Users").Column("IsCurrentSchool").Exists())
            {
                Delete.Column("IsCurrentSchool").FromTable("Users");
            }
            if (Schema.Table("Users").Column("ForeignKey").Exists())
            {
                Delete.Column("ForeignKey").FromTable("Users");
            }
            if (Schema.Table("Users").Column("Salutation").Exists())
            {
                Delete.Column("Salutation").FromTable("Users");
            }
            if (Schema.Table("Users").Column("ResetPasswordToken").Exists())
            {
                Delete.Column("ResetPasswordToken").FromTable("Users");
            }
            if (Schema.Table("Users").Column("AllowToContact").Exists())
            {
                Delete.Column("AllowToContact").FromTable("Users");
            }
            base.Down();
        }

        public override void Up()
        {
            if (!Schema.Table("Users").Column("ReferenceNumber").Exists())
            {
                Alter.Table("Users")
                .AddColumn("ReferenceNumber").AsString(255).Nullable();
            }
            if (!Schema.Table("Users").Column("IsAlreadyBooked").Exists())
            {
                Alter.Table("Users")
                .AddColumn("IsAlreadyBooked").AsInt32().WithDefaultValue(0);
            }
            if (!Schema.Table("Users").Column("LastBookedDate").Exists())
            {
                Alter.Table("Users")
                .AddColumn("LastBookedDate").AsDateTime().Nullable();
            }
            if (!Schema.Table("Users").Column("IsCurrentSchool").Exists())
            {
                Alter.Table("Users")
                .AddColumn("IsCurrentSchool").AsInt32().WithDefaultValue(0);
            }
            if (!Schema.Table("Users").Column("ForeignKey").Exists())
            {
                Alter.Table("Users")
                .AddColumn("ForeignKey").AsString(255).Nullable();
            }            
            if (!Schema.Table("Users").Column("Salutation").Exists())
            {
                Alter.Table("Users")
                .AddColumn("Salutation").AsString(100).Nullable();
            }
            if (!Schema.Table("Users").Column("ResetPasswordToken").Exists())
            {
                Alter.Table("Users")
                .AddColumn("ResetPasswordToken").AsString(100).Nullable();
            }
            if (!Schema.Table("Users").Column("AllowToContact").Exists())
            {
                Alter.Table("Users")
                .AddColumn("AllowToContact").AsInt32().WithDefaultValue(0);
            }
        }
    }
}
