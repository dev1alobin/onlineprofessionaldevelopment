﻿using AloFramework.Core.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Models
{
    public class OHPUserPrincipal : UserPrincipal
    {
        public OHPUserPrincipal(string Username)
            :base(Username)
        {

        }

        public string ReferenceNumber { get; set; }
    }
}
