﻿using AloFramework.Auth.Domain;
using AloFramework.DatabaseContext.Models.PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Models
{
    [TableName("Users")]
    [PrimaryKey("UserID")]
    public class OHPUser : User
    {
        public string ReferenceNumber { get; set; }
        public int IsAlreadyBooked { get; set; }
        public int IsCurrentSchool { get; set; }
        public string ForeignKey { get; set; }
        public string Salutation { get; set; }
        public string ResetPasswordToken { get; set; }
        public int AllowToContact { get; set; }
        public string UserType { get; set; }

        [ResultColumn]
        public string DisplayName
        {
            get
            {
                return (Salutation??"") + " " + FirstName + " " + Surname;
            }
        }
    }
}
