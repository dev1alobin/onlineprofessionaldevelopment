﻿using OHP.Plugin.Auth.Presentation.Models;
using OHP.Plugin.ParentRegistration.Domain;
using OHP.Plugin.StudentRegistration.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Repositories
{
    public interface IOHPUserSeederRepository
    {
        bool AddUser(OHPUserSeederModel ohpUserSeederModel);

        void AddParentInformation(ParentInformation parentInformationModel);

        bool AddStudentParentRelationship(OHPStudentParentRelationshipModel ohpStudentParentRelationshipModel);

        void AddStudentInformation(StudentInformation studentInformationModel);

        string GetParentUserIDFromStudentParentRelationship(string studentUserID);

        string GetUserNameFromUsers(string parentUserID);

        //void AddStudentIntoUsers(OHPUserSeederModel ohpUserSeederModel);
    }
}
