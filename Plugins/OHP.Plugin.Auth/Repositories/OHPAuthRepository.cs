﻿using AloFramework.Auth.Utilities;
using AloFramework.DatabaseContext.Models.PetaPoco;
using AloFramework.DatabaseContext.Repositories;
using OHP.Plugin.Auth.Models;
using OHP.Plugin.Auth.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Repositories
{
    public class OHPAuthRepository : Repository, IOHPAuthRepository
    {

        #region IOHPAuthRepository

        public OHPUser AuthenticateViaForeignKey(string foreignKey)
        {
            var query = new Sql("select * from Users where Username = @0", foreignKey);
            return this.dbContext.Fetch<OHPUser>(query).FirstOrDefault();
        }

        public void ResetPassword(string emailAddress, string resetPasswordToken)
        {
            var query = new Sql("select * from Users where EmailAddress = @0", emailAddress);
            var user = this.dbContext.Fetch<OHPUserModel>(query).FirstOrDefault();
            if (user != null)
            {
                this.dbContext.Execute("update Users set ResetPasswordToken = @0 where EmailAddress = @1", resetPasswordToken, user.EmailAddress);
                SessionUtil.Success("Passord reset link has been sent successfully");
            }
            else
            {
                SessionUtil.Warn("Email Address not found.");
            }
        }

        public void DoResetPassword(OHPResetPasswordModel ohpResetPasswordModel)
        {
            var query = new Sql("select * from Users where ResetPasswordToken = @0", ohpResetPasswordModel.ResetPasswordToken);
            var user = this.dbContext.Fetch<OHPUserModel>(query).FirstOrDefault();
            if (user != null)
            {
                this.dbContext.Execute("update Users set Password = @0, ResetPasswordToken = null where ResetPasswordToken = @1", ohpResetPasswordModel.Password, ohpResetPasswordModel.ResetPasswordToken);
                SessionUtil.Success("Password reset successfull");
            }
            else
            {
                SessionUtil.Warn("Password reset token is not valid, please try again");
            }
        }

        public OHPUserModel CheckUserExists(OHPUserModel ohpUserModel)
        {
            var query = new Sql("select * from Users where Username = @0 or EmailAddress = @1", ohpUserModel.Username, ohpUserModel.EmailAddress);
            return this.dbContext.Fetch<OHPUserModel>(query).FirstOrDefault();
        }

        public void UpdateUser(OHPUserModel ohpUserModel)
        {
            var query = new Sql("update Users set AllowToContact = @0, HearedAboutOHPFrom = @2 where Username = @1", ohpUserModel.AllowToContact, AuthUtil.Username, ohpUserModel.HearedAboutOHPFrom);
            this.dbContext.Execute(query);
        }

        public OHPUserModel GetUserByUsername(string username)
        {
            var query = new Sql("select * from Users where Username = @0", username);
            return this.dbContext.Fetch<OHPUserModel>(query).FirstOrDefault();
        }

        public OHPUserModel GetUserByForeignKey(string foreignKey)
        {
            var query = new Sql("select * from Users where ReferenceNumber = @0", foreignKey);
            return this.dbContext.Fetch<OHPUserModel>(query).FirstOrDefault();
        }


        #endregion
    }
}
