﻿using OHP.Plugin.Auth.Models;
using OHP.Plugin.Auth.Presentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OHP.Plugin.Auth.Repositories
{
    public interface IOHPAuthRepository
    {

        OHPUser AuthenticateViaForeignKey(string foreignKey);

        void ResetPassword(string emailAddress, string resetPasswordToken);

        void DoResetPassword(OHPResetPasswordModel ohpResetPasswordModel);

        OHPUserModel CheckUserExists(OHPUserModel ohpUserModel);

        void UpdateUser(OHPUserModel ohpUserModel);

        OHPUserModel GetUserByUsername(string username);

        OHPUserModel GetUserByForeignKey(string foreignKey);

    }
}
