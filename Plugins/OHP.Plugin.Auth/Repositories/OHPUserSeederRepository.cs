﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OHP.Plugin.Auth.Presentation.Models;
using AloFramework.DatabaseContext.Repositories;
using AloFramework.DatabaseContext.Models.PetaPoco;
using AloFramework.Core.Utilities;
using OHP.Plugin.ParentRegistration.Domain;
using OHP.Plugin.StudentRegistration.Domain;

namespace OHP.Plugin.Auth.Repositories
{
    public class OHPUserSeederRepository : Repository, IOHPUserSeederRepository
    {
        #region IOHPAuthRepository


        public bool AddUser(OHPUserSeederModel ohpUserSeederModel)
        {
            try
            {
                var query = new Sql("select * from Users where ForeignKey = @0", ohpUserSeederModel.ForeignKey);
                var user = this.dbContext.Fetch<OHPUserSeederModel>(query).FirstOrDefault();
                if (user == null)//.Count <= 0)
                {
                    this.dbContext.Execute(
                        @"INSERT INTO Users (Username, ForeignKey, ReferenceNumber, UserType, Salutation, FirstName, Surname, MobileNo, EmailAddress, CreatedAt, UpdatedAt, Password)
                                VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9, @10, @11);",
                                ohpUserSeederModel.Username,
                                ohpUserSeederModel.ForeignKey,
                                ohpUserSeederModel.ReferenceNumber,
                                ohpUserSeederModel.UserType,
                                ohpUserSeederModel.Salutation,
                                ohpUserSeederModel.FirstName,
                                ohpUserSeederModel.Surname,
                                ohpUserSeederModel.MobileNo,
                                ohpUserSeederModel.EmailAddress,
                                DateTime.Now,
                                DateTime.Now,
                                ohpUserSeederModel.Username
                        );
                }
                /*else {
                    this.dbContext.Execute(
                        @"Update Users set 
                        Username = @0, 
                        ForeignKey = @1, 
                        ReferenceNumber = @2, 
                        UserType = @3, 
                        Salutation = @4, 
                        FirstName = @5, 
                        Surname = @6, 
                        MobileNo = @7, 
                        EmailAddress = @8, 
                        CreatedAt = @9, 
                        UpdatedAt = @10, 
                        Password = @11 
                        where ForeignKey = @1",
                                ohpUserSeederModel.Username,
                                ohpUserSeederModel.ForeignKey,
                                ohpUserSeederModel.ReferenceNumber,
                                ohpUserSeederModel.UserType,
                                ohpUserSeederModel.Salutation,
                                ohpUserSeederModel.FirstName,
                                ohpUserSeederModel.Surname,
                                ohpUserSeederModel.MobileNo,
                                ohpUserSeederModel.EmailAddress,
                                DateTime.Now,
                                DateTime.Now,
                                ohpUserSeederModel.Username
                        );
                }*/
                LogUtil.Info("New User inserted , Username = " + ohpUserSeederModel.Username + ", CarerNumber = " + ohpUserSeederModel.ForeignKey + ", UserType = " + ohpUserSeederModel.UserType);
                return true;
            }
            catch (Exception ex) {
                LogUtil.Error("Problem in inserting new user, due to " + ex + ", UserType = " + ohpUserSeederModel.UserType);
            }
            return false;
        }

        public void AddParentInformation(ParentInformation parentInformation)
        {
            try
            {
                var parentInfoExists = this.dbContext.Fetch<ParentInformation>("select * from ParentInformations where ReferenceNumber = @0", parentInformation.ReferenceNumber).FirstOrDefault();
                if (parentInfoExists == null)
                {
                    this.dbContext.Insert(parentInformation);
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in inserting ParentInformation, due to " + ex + ", Username = " + parentInformation.Username);
            }
        }

        public bool AddStudentParentRelationship(OHPStudentParentRelationshipModel ohpStudentParentRelationshipModel)
        {
            try
            {
                var query = new Sql("select * from StudentParentRelatioships where StudentUserID = @0 and ParentUserID = @1", ohpStudentParentRelationshipModel.StudentUserID, ohpStudentParentRelationshipModel.ParentUserID);
                var record = this.dbContext.Fetch<OHPStudentParentRelationshipModel>(query).FirstOrDefault();
                if (record == null)
                {
                    this.dbContext.Execute(
                        @"INSERT INTO StudentParentRelatioships (StudentUserID, ParentUserID, Relationship)
                                VALUES (@0, @1, @2);",
                                ohpStudentParentRelationshipModel.StudentUserID,
                                ohpStudentParentRelationshipModel.ParentUserID,
                                ohpStudentParentRelationshipModel.Relationship
                        );
                    //this.dbContext.Insert(ohpStudentParentRelationshipModel);
                }
                return true;
            }
            catch (Exception ex) {
                LogUtil.Error("Problem in inserting StudentParentRelationship, due to " + ex + ", StudeUserID = " + ohpStudentParentRelationshipModel.StudentUserID);
            }
            return false;
        }

        public void AddStudentInformation(StudentInformation studentInformationModel)
        {
            try
            {
                LogUtil.Info("Foreign Key = " + studentInformationModel.ForeignKey);
                var parentUserID = this.GetParentUserIDFromStudentParentRelationship(studentInformationModel.ForeignKey);
                LogUtil.Info("Parent User ID = " + parentUserID);
                /*var querySPRelationships = new Sql("Select ForeignKey from StudentParentRelatioships where StudentUserID = @0", studentInformationModel.StudentUserID);
                var foreignKey = this.dbContext.Fetch<OHPStudentParentRelationshipModel>(querySPRelationships).FirstOrDefault();
                this.dbContext.AbortTransaction();*/
                if (parentUserID != null)
                {
                    /*var username = this.GetUserNameFromUsers(parentUserID);
                    LogUtil.Info("UserName = " + username);                   
                    if (username != null)
                    { */
                    studentInformationModel.Username = parentUserID;
                    this.dbContext.Insert(studentInformationModel);
                    //}
                }
            }
            catch (Exception ex)
            {
                LogUtil.Error("Problem in inserting StudentInformation, due to " + ex + ", StudentUserID = " + studentInformationModel.StudentUserID);
            }
        }

        public string GetParentUserIDFromStudentParentRelationship(string studentUserID)
        {
            var querySPRelationships = new Sql("Select ParentUserID from StudentParentRelatioships where StudentUserID = @0", studentUserID);
            var record = this.dbContext.Fetch<OHPStudentParentRelationshipModel>(querySPRelationships).FirstOrDefault();
            return record.ParentUserID;
        }

        public string GetUserNameFromUsers(string parentUserID)
        {
            var queryUser = new Sql("Select Username from Users where ForeignKey = @0", parentUserID);
            var record = this.dbContext.Fetch<OHPUserModel>(queryUser).FirstOrDefault();
            return record.Username;
        }

        #endregion
    }
}
